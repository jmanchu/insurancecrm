//
//  SFDCAccountDetailsViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/6/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCAccountDetailsViewController.h"
#import "SFDCCreatePolicyViewController.h"
#import "SFDCAppDelegate.h"

static MMSF_Object *editedAccount;
static MMSF_Object *selectedPolicyObj;

@interface SFDCAccountDetailsViewController ()
@property (nonatomic, retain) NSDictionary *originalAccountProperties;
@property (nonatomic, strong) UIView *intermediateView;
@property (nonatomic, strong) NSArray *policyListArray;
@property (nonatomic, strong) UIPopoverController *popoverController;
- (void)saveStringPropertiesFromAccount:(MMSF_Object *)accountToSave;
- (void)restorePropertiesIntoAccount:(MMSF_Object *)accountToRestore;

@end

@implementation SFDCAccountDetailsViewController
@synthesize originalAccountProperties, policyListArray, intermediateView, popoverController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(canProceedWithSaving:) name: kNotification_ObjectChangesSaved object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(objectSavingFailed:) name: kNotification_ObjectSaveError object: nil];
    
    [aScrollView setContentSize:CGSizeMake(320, 420)];
    [titleLabel setText:[editedAccount valueForKey:@"Name"]];
    if (editedAccount != nil) {
        self.originalAccountProperties = nil;
        [self saveStringPropertiesFromLead:editedAccount];
        
        [accountOwnerField setText:[[editedAccount owner] valueForKey:@"Name"]];
        [accountNameField setText:[editedAccount valueForKey:@"Name"]];
        [contactNumberField setText:[editedAccount valueForKey:@"Phone"]];
        [annualRevenueField setText:[NSString stringWithFormat:@"%ld",[[editedAccount valueForKey:@"AnnualRevenue"] longValue]]];
        [currentAddreeField setText:[self fullAddressOfAccount:editedAccount]];
        [permanentAddressField setText:[self currentAddressOfAccount:editedAccount]];
        
        [accountOwnerField setBorderStyle:UITextBorderStyleNone];
        [accountNameField  setBorderStyle:UITextBorderStyleNone];
        [contactNumberField  setBorderStyle:UITextBorderStyleNone];
        [annualRevenueField  setBorderStyle:UITextBorderStyleNone];
        [currentAddreeField  setBorderStyle:UITextBorderStyleNone];
        [permanentAddressField  setBorderStyle:UITextBorderStyleNone];
        
        [accountOwnerField setUserInteractionEnabled:NO];
        [accountNameField  setUserInteractionEnabled:NO];
        [contactNumberField  setUserInteractionEnabled:NO];
        [annualRevenueField  setUserInteractionEnabled:NO];
        [currentAddreeField  setUserInteractionEnabled:NO];
        [permanentAddressField  setUserInteractionEnabled:NO];
    }
    
    [self.view addSubview:cancelButton];
    [self.view addSubview:doneButton];
    [cancelButton setCenter:CGPointMake(34.0, 25.0)];
    [doneButton setCenter:CGPointMake(270.0, 26.0)];
    [cancelButton setAlpha:0.0];
    [doneButton setAlpha:0.0];
    
    NSManagedObjectContext *context = [editedAccount moc];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:$A($P(@"AccountId == %@",editedAccount), nil)];
    
	// Create the sort descriptors array.
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
    NSSortDescriptor *nameDesciptor = [NSSortDescriptor descriptorWithKey:@"ContractNumber" ascending: YES selector:@selector(caseInsensitiveCompare:)];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nameDesciptor, nil];
    
    self.policyListArray = [[context allObjectsOfType: @"Contract" matchingPredicate: predicate sortedBy:sortDescriptors] mutableCopy];
    
    
    /*
     * Add MyPickerView
     */
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self.view addSubview:myPickerView];
        CGRect myPickerViewFrame = [myPickerView frame];
        myPickerViewFrame.origin.x = 0;
        myPickerViewFrame.origin.y = 460;
        [myPickerView setFrame:myPickerViewFrame];
        
        self.intermediateView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [self.intermediateView setBackgroundColor:[UIColor blackColor]];
        [self.view insertSubview:self.intermediateView belowSubview:myPickerView];
        [self.intermediateView setAlpha:0.0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


+ (SFDCAccountDetailsViewController *)accountDetailsControllerForParent:(SFDCAccountsViewController *)parentViewCont withAccount:(MMSF_Object *)accountObj {
    NSString *nibName = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nibName = @"SFDCAccountDetailsViewController_iPad";
    } else {
        nibName = @"SFDCAccountDetailsViewController";
    }
    SFDCAccountDetailsViewController *accountDetailsViewController = [[SFDCAccountDetailsViewController alloc] initWithNibName:nibName bundle:nil];
    editedAccount = accountObj;
    selectedPolicyObj = nil;
    return accountDetailsViewController;
}


- (NSString *)fullAddressOfAccount:(MMSF_Object *)account {
    NSMutableString	*string = [NSMutableString stringWithString: [account valueForKey:@"BillingStreet"] ?: @""];
	if ([[account valueForKey:@"BillingCity"] length])
        [string appendFormat: @"%@%@", string.length ? @", " : @"", [account valueForKey:@"BillingCity"]];
    
	if ([[account valueForKey:@"BillingState"] length])
        [string appendFormat: @"%@%@", string.length ? @" " : @"", [account valueForKey:@"BillingState"]];
    
    if ([[account valueForKey:@"BillingCountry"] length])
        [string appendFormat: @"%@%@", string.length ? @" " : @"", [account valueForKey:@"BillingCountry"]];
    
	if ([[account valueForKey:@"BillingPostalCode"] length])
        [string appendFormat: @"%@%@", string.length ? @" " : @"", [account valueForKey:@"BillingPostalCode"]];
    
	[string replaceOccurrencesOfString: @"\n" withString: @" " options: 0 range: string.fullRange];
	return string;
}

- (NSString *)currentAddressOfAccount:(MMSF_Object *)account {
    NSMutableString	*string = [NSMutableString stringWithString: [account valueForKey:@"ShippingStreet"] ?: @""];
	if ([[account valueForKey:@"ShippingCity"] length])
        [string appendFormat: @"%@%@", string.length ? @", " : @"", [account valueForKey:@"ShippingCity"]];
    
	if ([[account valueForKey:@"ShippingState"] length])
        [string appendFormat: @"%@%@", string.length ? @" " : @"", [account valueForKey:@"ShippingState"]];
    
    if ([[account valueForKey:@"ShippingCountry"] length])
        [string appendFormat: @"%@%@", string.length ? @" " : @"", [account valueForKey:@"ShippingCountry"]];
    
	if ([[account valueForKey:@"ShippingPostalCode"] length])
        [string appendFormat: @"%@%@", string.length ? @" " : @"", [account valueForKey:@"ShippingPostalCode"]];
    
	[string replaceOccurrencesOfString: @"\n" withString: @" " options: 0 range: string.fullRange];
	return string;
}


#pragma mark - Notifications
- (void) canProceedWithSaving: (NSNotification *) note {
    originalAccountProperties = nil;
    editedAccount = nil;
    [self backButtonAction:nil];
}

- (void) objectSavingFailed: (NSNotification *) note {
    if (editedAccount) {
        [self restorePropertiesIntoLead:editedAccount];
        [editedAccount save];
        
        [accountOwnerField setText:[[editedAccount owner] valueForKey:@"Name"]];
        [accountNameField setText:[editedAccount valueForKey:@"Name"]];
        [contactNumberField setText:[editedAccount valueForKey:@"Phone"]];
        [annualRevenueField setText:[NSString stringWithFormat:@"%ld",[[editedAccount valueForKey:@"AnnualRevenue"] longValue]]];
        [currentAddreeField setText:[self fullAddressOfAccount:editedAccount]];
        [permanentAddressField setText:[self currentAddressOfAccount:editedAccount]];
    }
    
    /*
     * Print an alert with the reson for failing to save.
     */
}


- (void)saveStringPropertiesFromLead:(MMSF_Object *)leadToSave {
    originalAccountProperties =  [NSDictionary dictionaryWithObjectsAndKeys:
                               [[leadToSave owner] valueForKey:@"Name"], @"OwnerName",
                               [leadToSave valueForKey:@"Name"], @"Name",
                               [leadToSave valueForKey:@"Phone"], @"Phone",
                               [leadToSave valueForKey:@"AnnualRevenue"], @"AnnualRevenue",
                               nil];
}

- (void) restorePropertiesIntoLead:(MMSF_Object *)leadToRestore {
    if (originalAccountProperties != nil) {
        [leadToRestore setValue:[originalAccountProperties objectForKey:@"Name"] forKey:@"Name"];
        [leadToRestore setValue:[originalAccountProperties objectForKey:@"Phone"] forKey:@"Phone"];
        [leadToRestore setValue:[originalAccountProperties objectForKey:@"AnnualRevenue"] forKey:@"AnnualRevenue"];
    }
}

#pragma mark - Button Actions
- (void)showPopoverFromView:(UIButton *)view {
    if (!self.popoverController) {
        UIViewController* popoverContent = [[UIViewController alloc]init];
        popoverContent.view = myPickerView;
        popoverContent.contentSizeForViewInPopover = CGSizeMake(320, 266);
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:popoverContent];
    }
    [self.popoverController  presentPopoverFromRect:CGRectMake(view.frame.size.width/2, view.frame.size.height/2, 10, 10)
                                             inView:view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)policyListButtonAction:(id)sender {
    if (![self.policyListArray count]) {
        [SA_AlertView showAlertWithTitle:@"No Policies!" message:@"Create a Policy!!"];
    } else {
        /*
         * Show the list of policies.
         */
        [myPickerViewTitleLabel setText:@"Select Term"];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self showPopoverFromView:sender];
        } else {
            [self presentMyPickerView];
        }
    }
}

- (IBAction)createPolicyAction:(id)sender {
    SFDCCreatePolicyViewController *createPolicyViewController = [SFDCCreatePolicyViewController createPolicyViewControllerFrom:self modallyUsingAccount:editedAccount];
    [self presentViewController:createPolicyViewController animated:YES completion:NULL];
    [[createPolicyViewController titleLabel] setText:@"New Policy"];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editButtonAction:(id)sender {
    [editButton setAlpha:0.0];
    [syncButton setAlpha:0.0];
    [backButton setAlpha:0.0];
    
    [cancelButton setAlpha:1.0];
    [doneButton setAlpha:1.0];
    
    
    [accountNameField  setBorderStyle:UITextBorderStyleLine];
    [contactNumberField  setBorderStyle:UITextBorderStyleLine];
    [annualRevenueField  setBorderStyle:UITextBorderStyleLine];
    
    [accountNameField  setUserInteractionEnabled:YES];
    [contactNumberField  setUserInteractionEnabled:YES];
    [annualRevenueField  setUserInteractionEnabled:YES];
    
    [accountNameField becomeFirstResponder];
}

- (IBAction)doneButtonAction:(id)sender {
    if ([self validateDataFields]) {
        [editButton setAlpha:1.0];
        [syncButton setAlpha:1.0];
        [backButton setAlpha:1.0];
        
        [cancelButton setAlpha:0.0];
        [doneButton setAlpha:0.0];
    }
}

- (IBAction)cancelButtonAction:(id)sender {
    [editButton setAlpha:1.0];
    [syncButton setAlpha:1.0];
    [backButton setAlpha:1.0];
    
    [cancelButton setAlpha:0.0];
    [doneButton setAlpha:0.0];
    
    [accountOwnerField setBorderStyle:UITextBorderStyleNone];
    [accountNameField  setBorderStyle:UITextBorderStyleNone];
    [contactNumberField  setBorderStyle:UITextBorderStyleNone];
    [annualRevenueField  setBorderStyle:UITextBorderStyleNone];
    [currentAddreeField  setBorderStyle:UITextBorderStyleNone];
    [permanentAddressField  setBorderStyle:UITextBorderStyleNone];
    
    [accountOwnerField setUserInteractionEnabled:NO];
    [accountNameField  setUserInteractionEnabled:NO];
    [contactNumberField  setUserInteractionEnabled:NO];
    [annualRevenueField  setUserInteractionEnabled:NO];
    [currentAddreeField  setUserInteractionEnabled:NO];
    [permanentAddressField  setUserInteractionEnabled:NO];
}

- (void)hideMyPickerView {
    CGRect myPickerFrame = myPickerView.frame;
    myPickerFrame.origin.y = 460;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [myPickerView setFrame:myPickerFrame];
    } completion:^(BOOL finished) {
        [self.intermediateView setAlpha:0.0];
    }];
}

- (void)presentMyPickerView {
    CGRect myPickerFrame = myPickerView.frame;
    myPickerFrame.origin.y = 460 - myPickerFrame.size.height;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [myPickerView setFrame:myPickerFrame];
    } completion:^(BOOL finished) {
        [self.intermediateView setAlpha:0.2];
    }];
}

- (IBAction)myPickerCancelBtnAction:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    } else {
        [self hideMyPickerView];
    }
}

- (IBAction)myPickerShowBtnAction:(id)sender {
    if (!selectedPolicyObj) {
        selectedPolicyObj = (MMSF_Object *)[policyListArray objectAtIndex:0];
    }
    SFDCCreatePolicyViewController *viewPolicyViewController = [SFDCCreatePolicyViewController viewPolicyViewControllerFrom:self modallyUsingPolicy:selectedPolicyObj];
    [self.navigationController pushViewController:viewPolicyViewController animated:YES];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    } else {
        [self hideMyPickerView];
    }
}

- (IBAction)syncButtonAction:(id)sender {
    SFDCAppDelegate *appDel = (SFDCAppDelegate *)([[UIApplication sharedApplication]delegate]);
    [appDel refreshAction];
}

#pragma mark - Validation
- (BOOL) validatePhoneNumber:(NSString *)phoneNumber {
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                               error:nil];
    NSUInteger numberOfMatches = [detector numberOfMatchesInString:phoneNumber
                                                           options:0
                                                             range:NSMakeRange(0, [phoneNumber length])];
    if (numberOfMatches == 0) {
        return NO;
    }
    return YES;
}

- (BOOL) validateDataFields {
    
    if (![[accountNameField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Account Name!" message:@"Account Name should not be empty!!"];
        [accountNameField becomeFirstResponder];
        return NO;
    }
    if (![[contactNumberField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No contact Number!" message:@"Contact Number should not be empty!!"];
        [contactNumberField becomeFirstResponder];
        return NO;
    }
    if (![self validatePhoneNumber:[contactNumberField text]]) {
        [SA_AlertView showAlertWithTitle: @"Please enter a valid Contact Number" message: @""];
        [contactNumberField becomeFirstResponder];
        return NO;
    }
    if (![[annualRevenueField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Annual Revenue!" message:@"Annual Revenue should not be empty!!"];
        [annualRevenueField becomeFirstResponder];
        return NO;
    }
    
    [accountNameField  setBorderStyle:UITextBorderStyleNone];
    [contactNumberField  setBorderStyle:UITextBorderStyleNone];
    [annualRevenueField  setBorderStyle:UITextBorderStyleNone];
    
    [accountNameField  setUserInteractionEnabled:NO];
    [contactNumberField  setUserInteractionEnabled:NO];
    [annualRevenueField  setUserInteractionEnabled:NO];
    
    [editedAccount setValue:accountNameField.text forKey:@"Name"];
    [editedAccount setValue:contactNumberField.text forKey:@"Phone"];
    [editedAccount setValue:annualRevenueField.text forKey:@"AnnualRevenue"];
    [editedAccount setValue:[NSDate date] forKey:@"LastModifiedDate"];
    
    [editedAccount finishEditingSavingChanges:YES];
    return YES;
}

#pragma mark - UITextField Delegate
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField isEqual:annualRevenueField]) {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if(theTextField == accountOwnerField) {
        [accountNameField becomeFirstResponder];
    } else if(theTextField == accountNameField) {
        [contactNumberField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 40.0) animated:YES];
    } else if(theTextField == contactNumberField) {
        [annualRevenueField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 80.0) animated:YES];
    } else if(theTextField == annualRevenueField){
        [currentAddreeField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 160.0) animated:YES];
    } else if(theTextField == currentAddreeField) {
        [permanentAddressField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 200.0) animated:YES];
    } else if(theTextField == permanentAddressField) {
        [permanentAddressField resignFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}

#pragma mark - UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    selectedPolicyObj = (MMSF_Object *)[policyListArray objectAtIndex:row];
}


#pragma mark - UIPickerViewDataSource

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *returnStr = @"";
    returnStr = [(MMSF_Object *)[policyListArray objectAtIndex:row] valueForKey:@"ContractNumber"];
	return returnStr;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat componentWidth = 0.0;
    componentWidth = 320;
    return componentWidth;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger count;
    count = [policyListArray count];
    return count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

@end
