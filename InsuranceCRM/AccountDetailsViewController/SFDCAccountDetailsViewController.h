//
//  SFDCAccountDetailsViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/6/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SFDCAccountsViewController;

@interface SFDCAccountDetailsViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    IBOutlet UIButton *backButton;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIButton *syncButton;
    IBOutlet UIButton *editButton;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *doneButton;
    IBOutlet UIButton *policyListButton;
    IBOutlet UIScrollView *aScrollView;
    
    IBOutlet UITextField *accountOwnerField;
    IBOutlet UITextField *accountNameField;
    IBOutlet UITextField *contactNumberField;
    IBOutlet UITextField *annualRevenueField;
    IBOutlet UITextField *currentAddreeField;
    IBOutlet UITextField *permanentAddressField;

    IBOutlet UIView *myPickerView;
    IBOutlet UIButton *myPickerCancelBtn;
    IBOutlet UILabel *myPickerViewTitleLabel;
    IBOutlet UIButton *myPickerShowBtn;
    IBOutlet UIPickerView *policyListPicker;
}
- (IBAction)backButtonAction:(id)sender;
- (IBAction)syncButtonAction:(id)sender;
- (IBAction)editButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)doneButtonAction:(id)sender;
- (IBAction)policyListButtonAction:(id)sender;
- (IBAction)createPolicyAction:(id)sender;

- (IBAction)myPickerCancelBtnAction:(id)sender;
- (IBAction)myPickerShowBtnAction:(id)sender;

+ (SFDCAccountDetailsViewController *)accountDetailsControllerForParent:(SFDCAccountsViewController *)parentViewCont withAccount:(MMSF_Object *)accountObj;
@end
