//
//  SFDCHomeViewController_iPad.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 2/4/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCHomeViewController_iPad : UIViewController {
    
}
@property (strong, nonatomic) IBOutlet UIView *leftPaneView;
@property (strong, nonatomic) IBOutlet UIButton *leadsButton;
@property (strong, nonatomic) IBOutlet UIButton *accountsButton;
@property (strong, nonatomic) IBOutlet UIButton *policiesButton;
@property (strong, nonatomic) IBOutlet UIButton *productsButton;
@property (strong, nonatomic) IBOutlet UIButton *premiumCalButton;

- (IBAction)leadsAction:(id)sender;
- (IBAction)accountsAction:(id)sender;
- (IBAction)policiesAction:(id)sender;
- (IBAction)productsAction:(id)sender;
- (IBAction)premiumCalAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *rightPaneView;
@property (strong, nonatomic) IBOutlet UIImageView *calendarImgView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *titleImgView;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;

- (IBAction)syncButtonAction:(id)sender;
- (IBAction)settingsButtonAction:(id)sender;
- (IBAction)homeButtonAction:(id)sender;

@end
