//
//  SFDCHomeViewController_iPad.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 2/4/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCHomeViewController_iPad.h"
#import "SFDCLeadsViewController.h"
#import "SFDCAccountsViewController.h"
#import "SFDCCreatePolicyViewController.h"
#import "SFDCProductsViewController.h"
#import "SFDCPremiumCalViewController.h"
#import "SFDCAppDelegate.h"

@interface SFDCHomeViewController_iPad ()
@property (nonatomic, strong) SFDCLeadsViewController *leadsViewController;
@property (nonatomic, strong) SFDCAccountsViewController *accountsViewController;
@property (nonatomic, strong) SFDCCreatePolicyViewController *createPolicyViewController;
@property (nonatomic, strong) SFDCProductsViewController *productsViewController;
@property (nonatomic, strong) SFDCPremiumCalViewController *premiumCalViewController;

@end

@implementation SFDCHomeViewController_iPad
@synthesize leftPaneView, leadsButton, accountsButton, policiesButton, productsButton, premiumCalButton, rightPaneView, calendarImgView, leadsViewController, accountsViewController, createPolicyViewController, productsViewController, premiumCalViewController, homeButton, titleImgView;

#pragma mark - Init
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:rightPaneView];
    [self.view addSubview:leftPaneView];
    
    /*
     * Hide Left pane.
     */
    CGRect leftPaneFrame = leftPaneView.frame;
    leftPaneFrame.origin.x = - leftPaneFrame.size.width;
    leftPaneFrame.origin.y = 50;
    leftPaneView.frame = leftPaneFrame;
    
    /*
     * Hide Right pane.
     */
    CGRect rightPaneFrame = rightPaneView.frame;
    rightPaneFrame.origin.x = leftPaneView.frame.size.width;
    rightPaneFrame.origin.y = 50;
    rightPaneView.frame = rightPaneFrame;
    rightPaneView.alpha = 0.0;
    
    [self.homeButton setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CGRect leftPaneFrame = leftPaneView.frame;
    leftPaneFrame.origin.x = 0;
    float delay = 0.0;
    [UIView animateWithDuration:0.75 delay:delay options:UIViewAnimationCurveEaseIn animations:^{
        [leftPaneView setFrame:leftPaneFrame];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.75 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
            [rightPaneView setAlpha:1.0];
        } completion:NULL];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLeftPaneView:nil];
    [self setLeadsButton:nil];
    [self setAccountsButton:nil];
    [self setPoliciesButton:nil];
    [self setProductsButton:nil];
    [self setPremiumCalButton:nil];
    [self setRightPaneView:nil];
    [self setCalendarImgView:nil];
    [self setTitleLabel:nil];
    [self setTitleImgView:nil];
    [self setHomeButton:nil];
    [super viewDidUnload];
}

#pragma mark - Animations

- (void)animateScreenAndPush:(UIViewController *)viewController withTitle:(NSString *)titleString {
    /*
     * Animate & dismiss the left & right panes.
     */
    viewController.view.frame = rightPaneView.frame;
    CGRect originalViewFrame = viewController.view.frame;
    CGRect modifiedViewFrame = originalViewFrame;
    modifiedViewFrame.origin.x = - originalViewFrame.size.width;
    viewController.view.frame = modifiedViewFrame;
    [self.view insertSubview:viewController.view aboveSubview:rightPaneView];
    
    float delay = 0.5;
    [UIView animateWithDuration:0.75 delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
        [rightPaneView setAlpha:0.0];
        [leadsViewController.view setAlpha:0.0];
        [accountsViewController.view setAlpha:0.0];
        [createPolicyViewController.view setAlpha:0.0];
        [productsViewController.view setAlpha:0.0];
        [premiumCalViewController.view setAlpha:0.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.75 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
            [viewController.view setFrame:originalViewFrame];
            [viewController.view setAlpha:1.0];
        } completion:^(BOOL finished) {
            [self.titleLabel setText:titleString];
        }];
    }];
}

- (void)animateAndPresentView:(UIView *)view withTitle:(NSString *)titleString {
    [UIView animateWithDuration:0.75 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [rightPaneView setAlpha:0.0];
        [leadsViewController.view setAlpha:0.0];
        [accountsViewController.view setAlpha:0.0];
        [createPolicyViewController.view setAlpha:0.0];
        [productsViewController.view setAlpha:0.0];
        [premiumCalViewController.view setAlpha:0.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.75 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [view setAlpha:1.0];
        } completion:^(BOOL finished) {
            [self.titleLabel setText:titleString];
        }];
        
    }];
}

#pragma mark - Button Actions

- (IBAction)leadsAction:(id)sender {
    if (!leadsViewController) {
        leadsViewController = [[SFDCLeadsViewController alloc] initWithNibName:@"SFDCLeadsViewController_iPad" bundle:nil];
        [self addChildViewController:leadsViewController];
        [self animateScreenAndPush:leadsViewController withTitle:@"LEADS"];
    } else {
        [self animateAndPresentView:leadsViewController.view withTitle:@"LEADS"];
    }
    [[self titleImgView] setImage:[UIImage imageNamed:@"LEADS.png"]];
    [self.homeButton setHidden:NO];
}

- (IBAction)accountsAction:(id)sender {
    if (!accountsViewController) {
        accountsViewController = [[SFDCAccountsViewController alloc] initWithNibName:@"SFDCAccountsViewController_iPad" bundle:nil];
        [self addChildViewController:accountsViewController];
        [self animateScreenAndPush:accountsViewController withTitle:@"ACCOUNTS"];
    } else {
        [self animateAndPresentView:accountsViewController.view withTitle:@"ACCOUNTS"];
    }
//    [[self titleImgView] setImage:[UIImage imageNamed:@""]];
    [self.homeButton setHidden:NO];
}

- (IBAction)policiesAction:(id)sender {
    [self setCreatePolicyViewController:nil];
    if (!createPolicyViewController) {
        createPolicyViewController = [SFDCCreatePolicyViewController createPolicyViewControllerFrom:self];
        [self addChildViewController:createPolicyViewController];
        [self animateScreenAndPush:createPolicyViewController withTitle:@"CREATE POLICY"];
    } else {
        [self animateAndPresentView:createPolicyViewController.view withTitle:@"CREATE POLICY"];
    }
    [[self titleImgView] setImage:[UIImage imageNamed:@"NEW POLICY.png"]];
    [self.homeButton setHidden:NO];
}

- (IBAction)productsAction:(id)sender {
    if (!productsViewController) {
        productsViewController = [[SFDCProductsViewController alloc] initWithNibName:@"SFDCProductsViewController_iPad" bundle:nil];
        [self addChildViewController:productsViewController];
        [self animateScreenAndPush:productsViewController withTitle:@"PRODUCTS"];
    } else {
        [self animateAndPresentView:productsViewController.view withTitle:@"PRODUCTS"];
    }
    [[self titleImgView] setImage:[UIImage imageNamed:@"PRODUCTS.png"]];
    [self.homeButton setHidden:NO];
}

- (IBAction)premiumCalAction:(id)sender {
    if (!premiumCalViewController) {
        premiumCalViewController = [[SFDCPremiumCalViewController alloc] initWithNibName:@"SFDCPremiumCalViewController_iPad" bundle:nil];
        [self addChildViewController:premiumCalViewController];
        [self animateScreenAndPush:premiumCalViewController withTitle:@"PREMIUM CALCULATOR"];
    } else {
        [self animateAndPresentView:premiumCalViewController.view withTitle:@"PREMIUM CALCULATOR"];
    }
    [[self titleImgView] setImage:[UIImage imageNamed:@"PREMIUM  CALCULATOR.png"]];
    [self.homeButton setHidden:NO];
}

- (IBAction)syncButtonAction:(id)sender {
    SFDCAppDelegate *appDel = (SFDCAppDelegate *)([[UIApplication sharedApplication]delegate]);
    [appDel refreshAction];
}

- (IBAction)settingsButtonAction:(id)sender {
}

- (IBAction)homeButtonAction:(id)sender {
    [self animateAndPresentView:rightPaneView withTitle:@"GREEN INSURANCE"];
    [[self titleImgView] setImage:[UIImage imageNamed:@"GreenInsurance_Title.png"]];
    [self.homeButton setHidden:YES];
}
@end
