//
//  SFDCDigitalSignatureViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/7/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCDigitalSignatureViewController.h"
#import "PaintingView.h"
#import "SoundEffect.h"

#define kPaletteHeight			30
#define kPaletteSize			5
#define kMinEraseInterval		0.5

// Padding for margins
#define kLeftMargin				10.0
#define kTopMargin				10.0
#define kRightMargin			10.0

static int thumbNailTag;
static MMSF_Object *contractObj;
static MMSF_Object *newAttachment;
//FUNCTIONS:
/*
 HSL2RGB Converts hue, saturation, luminance values to the equivalent red, green and blue values.
 For details on this conversion, see Fundamentals of Interactive Computer Graphics by Foley and van Dam (1982, Addison and Wesley)
 You can also find HSL to RGB conversion algorithms by searching the Internet.
 See also http://en.wikipedia.org/wiki/HSV_color_space for a theoretical explanation
 */
static void HSL2RGB(float h, float s, float l, float* outR, float* outG, float* outB)
{
	float			temp1,
    temp2;
	float			temp[3];
	int				i;
	
	// Check for saturation. If there isn't any just return the luminance value for each, which results in gray.
	if(s == 0.0) {
		if(outR)
			*outR = l;
		if(outG)
			*outG = l;
		if(outB)
			*outB = l;
		return;
	}
	
	// Test for luminance and compute temporary values based on luminance and saturation
	if(l < 0.5)
		temp2 = l * (1.0 + s);
	else
		temp2 = l + s - l * s;
    temp1 = 2.0 * l - temp2;
	
	// Compute intermediate values based on hue
	temp[0] = h + 1.0 / 3.0;
	temp[1] = h;
	temp[2] = h - 1.0 / 3.0;
    
	for(i = 0; i < 3; ++i) {
		
		// Adjust the range
		if(temp[i] < 0.0)
			temp[i] += 1.0;
		if(temp[i] > 1.0)
			temp[i] -= 1.0;
		
		
		if(6.0 * temp[i] < 1.0)
			temp[i] = temp1 + (temp2 - temp1) * 6.0 * temp[i];
		else {
			if(2.0 * temp[i] < 1.0)
				temp[i] = temp2;
			else {
				if(3.0 * temp[i] < 2.0)
					temp[i] = temp1 + (temp2 - temp1) * ((2.0 / 3.0) - temp[i]) * 6.0;
				else
					temp[i] = temp1;
			}
		}
	}
	
	// Assign temporary values to R, G, B
	if(outR)
		*outR = temp[0];
	if(outG)
		*outG = temp[1];
	if(outB)
		*outB = temp[2];
}

@interface SFDCDigitalSignatureViewController () {
    NSMutableArray *filesCreated;
    NSMutableArray *existingAttachments;
}
@property (nonatomic, strong) NSMutableArray *filesCreated;
@property (nonatomic, strong) NSMutableArray *existingAttachments;
@end

@implementation SFDCDigitalSignatureViewController
@synthesize drawingView, filesCreated, existingAttachments;

+ (id)createDigitalSignatureControllerUsingParentId:(MMSF_Object *)parentIdObj {
    NSString *nibString = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nibString = @"SFDCDigitalSignatureViewController_iPad";
    } else {
        nibString = @"SFDCDigitalSignatureViewController";
    }
    SFDCDigitalSignatureViewController *digitalSignViewController = [[SFDCDigitalSignatureViewController alloc] initWithNibName:nibString bundle:nil];
    contractObj = parentIdObj;
    newAttachment = nil;
    thumbNailTag = 1;
    return digitalSignViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)getExistingAttachments {
    NSManagedObjectContext *context = [contractObj moc];
//    MMSF_Object *currentUser = [MM_SyncManager currentUserInContext:context];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:$A($P(@"ParentId == %@", [contractObj valueForKey:@"Id"]), nil)];
	// Create the sort descriptors array.
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nil];
    self.existingAttachments = [[context allObjectsOfType: @"Attachment" matchingPredicate: predicate sortedBy:sortDescriptors] mutableCopy];
}

- (void)createThumbnails {
    int k = 0;
    for (int i = 0; i < [self.existingAttachments count]; i++) {
        NSString *fileName = [(MMSF_Object *)[existingAttachments objectAtIndex:i] valueForKey:@"Name"];
        if ([[[fileName componentsSeparatedByString:@"_"] firstObject] isEqualToString:@"Sign"]) {
            NSString *imagePath = [(MMSF_Object *)[existingAttachments objectAtIndex:i] pathForDataField:@"Body"];
            UIImage *imageObj = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:imagePath]];
            UIImageView *imageThumbnail = [[UIImageView alloc] initWithImage:imageObj];
            [imageThumbnail setFrame:CGRectMake((k) * (existingSingsScrollView.frame.size.height + 5), 0.0, existingSingsScrollView.frame.size.height, existingSingsScrollView.frame.size.height)];
            [existingSingsScrollView addSubview:imageThumbnail];
            [existingSingsScrollView setContentSize:CGSizeMake(imageThumbnail.frame.origin.x+imageThumbnail.frame.size.width,imageThumbnail.frame.size.height)];
            k ++;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(canProceedWithSaving:) name: kNotification_ObjectCreated object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(objectSavingFailed:) name: kNotification_ObjectSaveError object: nil];

    // Do any additional setup after loading the view from its nib.
    CGRect					rect = [[UIScreen mainScreen] applicationFrame];
	CGFloat					components[3];
	
	// Create a segmented control so that the user can choose the brush color.
	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:
											[NSArray arrayWithObjects:
                                             [UIImage imageNamed:@"Red.png"],
                                             [UIImage imageNamed:@"Yellow.png"],
                                             [UIImage imageNamed:@"Green.png"],
                                             [UIImage imageNamed:@"Blue.png"],
                                             [UIImage imageNamed:@"Purple.png"],
                                             nil]];
	
	// Compute a rectangle that is positioned correctly for the segmented control you'll use as a brush color palette
	CGRect frame = CGRectMake(rect.origin.x + kLeftMargin, rect.size.height - kPaletteHeight - kTopMargin, rect.size.width - (kLeftMargin + kRightMargin), kPaletteHeight);
	segmentedControl.frame = frame;
	// When the user chooses a color, the method changeBrushColor: is called.
	[segmentedControl addTarget:self action:@selector(changeBrushColor:) forControlEvents:UIControlEventValueChanged];
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	// Make sure the color of the color complements the black background
	segmentedControl.tintColor = [UIColor darkGrayColor];
	// Set the third color (index values start at 0)
	segmentedControl.selectedSegmentIndex = 2;
    
    [self.view addSubview:segmentedControl];
    
    // Define a starting color
	HSL2RGB((CGFloat) 2.0 / (CGFloat)kPaletteSize, kSaturation, kLuminosity, &components[0], &components[1], &components[2]);
	// Set the color using OpenGL
	glColor4f(components[0], components[1], components[2], kBrushOpacity);
    
	// Load the sounds
	NSBundle *mainBundle = [NSBundle mainBundle];
	erasingSound = [[SoundEffect alloc] initWithContentsOfFile:[mainBundle pathForResource:@"Erase" ofType:@"caf"]];
	selectSound =  [[SoundEffect alloc] initWithContentsOfFile:[mainBundle pathForResource:@"Select" ofType:@"caf"]];
    
	// Erase the view when recieving a notification named "shake" from the NSNotificationCenter object
	// The "shake" nofification is posted by the PaintingWindow object when user shakes the device
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eraseView) name:@"shake" object:nil];
    
    thumbNailTag = 1;
    [publishButton setEnabled:NO];
    
    [self getExistingAttachments];
    if ([self.existingAttachments count]) {
        [self createThumbnails];
    }
}

// Change the brush color
- (void)changeBrushColor:(id)sender
{
 	CGFloat					components[3];
    
	//Play sound
 	[selectSound play];
	
	//Set the new brush color
 	HSL2RGB((CGFloat)[sender selectedSegmentIndex] / (CGFloat)kPaletteSize, kSaturation, kLuminosity, &components[0], &components[1], &components[2]);
 	glColor4f(components[0], components[1], components[2], kBrushOpacity);
}

// Called when receiving the "shake" notification; plays the erase sound and redraws the view
-(void) eraseView
{
	if(CFAbsoluteTimeGetCurrent() > lastTime + kMinEraseInterval) {
		[erasingSound play];
		[drawingView erase];
		lastTime = CFAbsoluteTimeGetCurrent();
	}
}

- (NSString *)currentDateTime {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy.MM.dd"];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH.mm.ss"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    NSString *theTime = [timeFormat stringFromDate:now];
    return [NSString stringWithFormat:@"Sign_%@_%@",theDate,theTime];
}

- (NSString *)fileNameComponent {
    // Check if the directory already exists
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directory = [NSString stringWithFormat:@"%@/Attachments/Signatures", [paths objectAtIndex:0]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory]) {
        // Directory does not exist so create it
        [[NSFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return [NSString stringWithFormat:@"Documents/Attachments/Signatures/%@.jpeg",[self currentDateTime]];
}

- (UIImageView *)createThumbnailForImage:(UIImage *)image {
    UIImageView *imageThumbnail = [[UIImageView alloc] initWithImage:image];
    [imageThumbnail setFrame:CGRectMake((thumbNailTag-1) * (newSignsScrollView.frame.size.height + 5), 0.0, newSignsScrollView.frame.size.height, newSignsScrollView.frame.size.height)];
    [newSignsScrollView addSubview:imageThumbnail];
    thumbNailTag ++;
    [newSignsScrollView setContentSize:CGSizeMake(imageThumbnail.frame.origin.x+imageThumbnail.frame.size.width,imageThumbnail.frame.size.height)];
    return imageThumbnail;
}

- (IBAction)captureAction:(id)sender {
    if (!filesCreated) {
        filesCreated = [[NSMutableArray alloc] init];
        [publishButton setEnabled:YES];
    }
    NSString *filePath = [self fileNameComponent];
    
    [filesCreated addObject:filePath];
	UIImage *image = [drawingView imageRepresentation];
    
    // Convert UIImage to JPEG
    NSData *imgData = UIImageJPEGRepresentation(image, 1);
    
    // Identify the home directory and file name
    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:filePath];
    
    // Write the file.  Choose YES atomically to enforce an all or none write. Use the NO flag if partially written files are okay which can occur in cases of corruption
    [imgData writeToFile:jpgPath atomically:YES];
    [self createThumbnailForImage:image];
}

- (IBAction)clear:(id)sender {
    [self eraseView];
}


- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)publishButtonAction:(id)sender {
    /*
     * Push file as an attachement to SFDC 'Attachment' object.
     */
    NSString *filePath = [filesCreated objectAtIndex:0];
    NSString *fileName = [[(NSString *)filePath componentsSeparatedByString:@"/"] lastObject];
    NSString  *fullPath = [NSHomeDirectory() stringByAppendingPathComponent:filePath];
//    UIImage *image = [UIImage imageWithContentsOfFile:fullPath];
//    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    NSManagedObjectContext *context = [contractObj moc];
    newAttachment = (MMSF_Object*)[context insertNewEntityWithName:@"Attachment"];
    [newAttachment beginEditing];
    MMSF_Object *currentUser = [MM_SyncManager currentUserInContext:context];
    [newAttachment setValue:currentUser forKey:@"OwnerId"];
    [newAttachment setValue:[contractObj valueForKey:@"Id"] forKey:@"ParentId"];
    [newAttachment setValue:fileName forKey:@"Name"];
    [newAttachment setValue:[NSData dataWithContentsOfFile:fullPath] forKey:@"Body"];
    [newAttachment setValue:@"image/jpeg" forKey:@"ContentType"];
    
    [newAttachment finishEditingSavingChanges:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Notifications
- (void) canProceedWithSaving: (NSNotification *) note {
    /*
     * Attachement pushed to SFDC, move the file from new files to existing files.
     */
    [SA_AlertView showAlertWithTitle:@"Digital Signature is posted successfully!" message:nil];
}

- (void) objectSavingFailed: (NSNotification *) note {
    if (newAttachment != nil) {
        [newAttachment deleteFromContext];
        newAttachment = nil;
    }
}


# pragma mark - Motion Gestures
- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
	if (motion == UIEventSubtypeMotionShake )
	{
		// User was shaking the device. Post a notification named "shake".
		[[NSNotificationCenter defaultCenter] postNotificationName:@"shake" object:self];
	}
}

- (void)motionCancelled:(UIEventSubtype)motion withEvent:(UIEvent *)event {
}



@end
