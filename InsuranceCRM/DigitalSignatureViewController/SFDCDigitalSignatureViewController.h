//
//  SFDCDigitalSignatureViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/7/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PaintingView;
@class SoundEffect;

@interface SFDCDigitalSignatureViewController : UIViewController {
	PaintingView		*drawingView;
    
	SoundEffect			*erasingSound;
	SoundEffect			*selectSound;
	CFTimeInterval		lastTime;
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *publishButton;
    IBOutlet UIButton *captureButton;
    IBOutlet UIScrollView *existingSingsScrollView;
    IBOutlet UIScrollView *newSignsScrollView;
}
@property (nonatomic, retain) IBOutlet PaintingView *drawingView;
- (IBAction)captureAction:(id)sender;
- (IBAction)clear:(id)sender;

- (IBAction)backButtonAction:(id)sender;
- (IBAction)publishButtonAction:(id)sender;

+ (id)createDigitalSignatureControllerUsingParentId:(MMSF_Object *)parentIdObj;

@end
