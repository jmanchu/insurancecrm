//
//  SFDCCaptureImagesViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/7/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCCaptureImagesViewController : UIViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {

    IBOutlet UILabel *titleLbl;
    IBOutlet UIButton *publishButton;
    IBOutlet UIButton *captureButton;
    IBOutlet UIScrollView *existingSignsScrollView;
    IBOutlet UIScrollView *newSignsScrollView;
}
- (IBAction)captureIamgeAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)publishButtonAction:(id)sender;

+ (SFDCCaptureImagesViewController *)createControllerWithTitle:(NSString *)title withContractObj:(MMSF_Object *)policyObj;
@property (nonatomic, strong) UILabel *titleLbl;

@end
