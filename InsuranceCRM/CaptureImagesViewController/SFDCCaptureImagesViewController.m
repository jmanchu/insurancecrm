//
//  SFDCCaptureImagesViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/7/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCCaptureImagesViewController.h"

BOOL _sourceTypeCamera = FALSE;
UIImage *selectedPhoto;

static int thumbNailTag;
static MMSF_Object *contractObj;
static MMSF_Object *newAttachment;

@interface SFDCCaptureImagesViewController ()
@property (nonatomic, strong) NSMutableArray *filesCreated;
@property (nonatomic, strong) NSMutableArray *existingAttachments;
@property (nonatomic, strong) UIPopoverController *popoverController;
@end

@implementation SFDCCaptureImagesViewController
@synthesize titleLbl, filesCreated, existingAttachments, popoverController;

+ (SFDCCaptureImagesViewController *)createControllerWithTitle:(NSString *)title withContractObj:(MMSF_Object *)policyObj {
    NSString *nibString = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nibString =@"SFDCCaptureImagesViewController_iPad";
    } else {
        nibString =@"SFDCCaptureImagesViewController";
    }
    SFDCCaptureImagesViewController *captureImgViewController = [[SFDCCaptureImagesViewController alloc] initWithNibName:nibString bundle:nil];
    contractObj = policyObj;
    newAttachment = nil;
    thumbNailTag = 1;
    [[captureImgViewController titleLbl] setText:title];
    return captureImgViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(canProceedWithSaving:) name: kNotification_ObjectCreated object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(objectSavingFailed:) name: kNotification_ObjectSaveError object: nil];

    thumbNailTag = 1;
    [publishButton setEnabled:NO];
    
    [self getExistingAttachments];
    if ([self.existingAttachments count]) {
        [self createThumbnails];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions
- (void)getExistingAttachments {
    NSManagedObjectContext *context = [contractObj moc];
    //    MMSF_Object *currentUser = [MM_SyncManager currentUserInContext:context];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:$A($P(@"ParentId == %@", [contractObj valueForKey:@"Id"]), nil)];
	// Create the sort descriptors array.
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nil];
    self.existingAttachments = [[context allObjectsOfType: @"Attachment" matchingPredicate: predicate sortedBy:sortDescriptors] mutableCopy];
}

- (void)createThumbnails {
    int k = 0;
    for (int i = 0; i < [self.existingAttachments count]; i++) {
        NSString *fileName = [(MMSF_Object *)[existingAttachments objectAtIndex:i] valueForKey:@"Name"];
        if ([[[fileName componentsSeparatedByString:@"_"] firstObject] isEqualToString:@"Photo"]) {
            NSString *imagePath = [(MMSF_Object *)[existingAttachments objectAtIndex:i] pathForDataField:@"Body"];
            UIImage *imageObj = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:imagePath]];
            UIImageView *imageThumbnail = [[UIImageView alloc] initWithImage:imageObj];
            [imageThumbnail setFrame:CGRectMake((k) * (existingSignsScrollView.frame.size.height + 5), 0.0, existingSignsScrollView.frame.size.height, existingSignsScrollView.frame.size.height)];
            [existingSignsScrollView addSubview:imageThumbnail];
            [existingSignsScrollView setContentSize:CGSizeMake(imageThumbnail.frame.origin.x+imageThumbnail.frame.size.width,imageThumbnail.frame.size.height)];
            k ++;
        }
    }
}

- (NSString *)currentDateTime {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy.MM.dd"];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH.mm.ss"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    NSString *theTime = [timeFormat stringFromDate:now];
    return [NSString stringWithFormat:@"Photo_%@_%@",theDate,theTime];
}

- (NSString *)fileNameComponent {
    // Check if the directory already exists
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directory = [NSString stringWithFormat:@"%@/Attachments/Photos", [paths objectAtIndex:0]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory]) {
        // Directory does not exist so create it
        [[NSFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return [NSString stringWithFormat:@"Documents/Attachments/Photos/%@.jpeg",[self currentDateTime]];
}

- (UIImageView *)createThumbnailForLocalFile:(UIImage *)image {
    UIImageView *imageThumbnail = [[UIImageView alloc] initWithImage:image];
    [imageThumbnail setFrame:CGRectMake((thumbNailTag-1) * (newSignsScrollView.frame.size.height + 5), 0.0, newSignsScrollView.frame.size.height, newSignsScrollView.frame.size.height)];
    [newSignsScrollView addSubview:imageThumbnail];
    thumbNailTag ++;
    [newSignsScrollView setContentSize:CGSizeMake(imageThumbnail.frame.origin.x+imageThumbnail.frame.size.width,imageThumbnail.frame.size.height)];
    return imageThumbnail;
}

- (IBAction)captureIamgeAction:(id)sender {
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Photos" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Choose Photo",@"Take Photo",nil];
	popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
	[popupQuery showInView:self.view];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)publishButtonAction:(id)sender {
    NSString *filePath = [filesCreated objectAtIndex:0];
    NSString *fileName = [[(NSString *)filePath componentsSeparatedByString:@"/"] lastObject];
    NSString  *fullPath = [NSHomeDirectory() stringByAppendingPathComponent:filePath];
    //    UIImage *image = [UIImage imageWithContentsOfFile:fullPath];
    //    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    NSManagedObjectContext *context = [contractObj moc];
    newAttachment = (MMSF_Object*)[context insertNewEntityWithName:@"Attachment"];
    [newAttachment beginEditing];
    MMSF_Object *currentUser = [MM_SyncManager currentUserInContext:context];
    [newAttachment setValue:currentUser forKey:@"OwnerId"];
    [newAttachment setValue:[contractObj valueForKey:@"Id"] forKey:@"ParentId"];
    [newAttachment setValue:fileName forKey:@"Name"];
    [newAttachment setValue:[NSData dataWithContentsOfFile:fullPath] forKey:@"Body"];
    [newAttachment setValue:@"image/jpeg" forKey:@"ContentType"];
    
    [newAttachment finishEditingSavingChanges:YES];
}
#pragma mark - Notifications
- (void) canProceedWithSaving: (NSNotification *) note {
    /*
     * Attachement pushed to SFDC, move the file from new files to existing files.
     */
    [SA_AlertView showAlertWithTitle:@"Digital Signature is posted successfully!" message:nil];
}

- (void) objectSavingFailed: (NSNotification *) note {
    if (newAttachment != nil) {
        [newAttachment deleteFromContext];
        newAttachment = nil;
    }
}

#pragma mark UIActionSheetDelegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	
	NSLog(@"buttonIndex === %d", buttonIndex);
	
	if(buttonIndex == 0) {
		_sourceTypeCamera = FALSE;
		picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            self.popoverController = [[UIPopoverController alloc] initWithContentViewController:picker];
//            popoverController.delegate=self;
            [popoverController presentPopoverFromRect:captureButton.bounds inView:captureButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        } else {
            [self presentModalViewController:picker animated:YES];
        }
		
	} else if (buttonIndex == 1 )
    {
#if TARGET_IPHONE_SIMULATOR
        {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert!!" message:@"Simulator cont launch camera" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
            [alert show];
        }
#else
        {
            // Device
            _sourceTypeCamera = TRUE;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentModalViewController:picker animated:YES];
        }
#endif
        
	} else {
		[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
	}
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	[picker dismissModalViewControllerAnimated:YES];
    selectedPhoto = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
	[NSThread detachNewThreadSelector:@selector(createThumbnailForImage:) toTarget:self withObject:selectedPhoto];
}

- (void)createThumbnailForImage:(UIImage *)image {
    /*
     * Save the image to desk.
     */
    
    if (!filesCreated) {
        filesCreated = [[NSMutableArray alloc] init];
        [publishButton setEnabled:YES];
    }
    NSString *filePath = [self fileNameComponent];
    
    [filesCreated addObject:filePath];
    
    // Convert UIImage to JPEG
    NSData *imgData = UIImageJPEGRepresentation(image, 1);
    
    // Identify the home directory and file name
    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:filePath];
    
    // Write the file.  Choose YES atomically to enforce an all or none write. Use the NO flag if partially written files are okay which can occur in cases of corruption
    [imgData writeToFile:jpgPath atomically:YES];
    [self createThumbnailForLocalFile:image];
}



@end
