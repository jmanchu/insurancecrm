//
//  SFDCLeadDetailsViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCLeadDetailsViewController.h"
#import "SFDCLeadsViewController.h"

static MMSF_Object *editedLead;
static SFDCLeadsViewController *leadsViewController;

@interface SFDCLeadDetailsViewController ()
@property (nonatomic, retain) NSDictionary *originalLeadProperties;
- (void)saveStringPropertiesFromLead:(MMSF_Object *)leadToSave;
- (void)restorePropertiesIntoLead:(MMSF_Object *)leadToRestore;
@end

@implementation SFDCLeadDetailsViewController
@synthesize originalLeadProperties;

+ (SFDCLeadDetailsViewController *)createControllerForParent:(id)parentController usingLead:(MMSF_Object *)leadObj {
    editedLead = leadObj;
    
    NSString *nibName = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nibName = @"SFDCLeadDetailsViewController_iPad";
    } else {
        nibName = @"SFDCLeadDetailsViewController";
    }
    SFDCLeadDetailsViewController *leadDetailViewController = [[SFDCLeadDetailsViewController alloc] initWithNibName:nibName bundle:nil];
    leadsViewController = parentController;
    return leadDetailViewController;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(canProceedWithSaving:) name: kNotification_ObjectChangesSaved object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(objectSavingFailed:) name: kNotification_ObjectSaveError object: nil];
    
    [aScrollView setContentSize:CGSizeMake(320, 520)];
    [titleLabel setText:[editedLead valueForKey:@"Name"]];
    if (editedLead != nil) {
        self.originalLeadProperties = nil;
        [self saveStringPropertiesFromLead:editedLead];
        
        [firstNameTextField setText:[editedLead valueForKey:@"FirstName"]];
        [lastNameTextField setText:[editedLead valueForKey:@"LastName"]];
        [contactNumberTextField setText:[editedLead valueForKey:@"Phone"]];
        [companyTextfield setText:[editedLead valueForKey:@"Company"]];
        [mailTextField setText:[editedLead valueForKey:@"Email"]];
        [streetTextField setText:[editedLead valueForKey:@"Street"]];
        [cityTextField setText:[editedLead valueForKey:@"City"]];
        [stateTextField setText:[editedLead valueForKey:@"State"]];
        [postalCodeTextField setText:[editedLead valueForKey:@"PostalCode"]];
        [countryTextField setText:[editedLead valueForKey:@"Country"]];
        
        [firstNameTextField setBorderStyle:UITextBorderStyleNone];
        [lastNameTextField  setBorderStyle:UITextBorderStyleNone];
        [contactNumberTextField  setBorderStyle:UITextBorderStyleNone];
        [companyTextfield  setBorderStyle:UITextBorderStyleNone];
        [mailTextField  setBorderStyle:UITextBorderStyleNone];
        [streetTextField  setBorderStyle:UITextBorderStyleNone];
        [cityTextField  setBorderStyle:UITextBorderStyleNone];
        [stateTextField  setBorderStyle:UITextBorderStyleNone];
        [postalCodeTextField  setBorderStyle:UITextBorderStyleNone];
        [countryTextField  setBorderStyle:UITextBorderStyleNone];
        
        [firstNameTextField setUserInteractionEnabled:NO];
        [lastNameTextField  setUserInteractionEnabled:NO];
        [contactNumberTextField  setUserInteractionEnabled:NO];
        [companyTextfield  setUserInteractionEnabled:NO];
        [mailTextField  setUserInteractionEnabled:NO];
        [streetTextField  setUserInteractionEnabled:NO];
        [cityTextField  setUserInteractionEnabled:NO];
        [stateTextField  setUserInteractionEnabled:NO];
        [postalCodeTextField  setUserInteractionEnabled:NO];
        [countryTextField  setUserInteractionEnabled:NO];
    }
    
    [self.view addSubview:cancelButton];
    [cancelButton setCenter:CGPointMake(804.0, 25.0)];
    [cancelButton setAlpha:0.0];
    [backButton setAlpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Notifications
- (void) canProceedWithSaving: (NSNotification *) note {
    originalLeadProperties = nil;
    editedLead = nil;
    [self backAction:nil];
}

- (void) objectSavingFailed: (NSNotification *) note {
    if (editedLead) {
        [self restorePropertiesIntoLead:editedLead];
        [editedLead save];
        
        [firstNameTextField setText:[editedLead valueForKey:@"FirstName"]];
        [lastNameTextField setText:[editedLead valueForKey:@"LastName"]];
        [contactNumberTextField setText:[editedLead valueForKey:@"Phone"]];
        [companyTextfield setText:[editedLead valueForKey:@"Company"]];
        [mailTextField setText:[editedLead valueForKey:@"Email"]];
        [streetTextField setText:[editedLead valueForKey:@"Street"]];
        [cityTextField setText:[editedLead valueForKey:@"City"]];
        [stateTextField setText:[editedLead valueForKey:@"State"]];
        [postalCodeTextField setText:[editedLead valueForKey:@"PostalCode"]];
        [countryTextField setText:[editedLead valueForKey:@"Country"]];
    }
    
    /*
     * Print an alert with the reson for failing to save.
     */
}



- (void)saveStringPropertiesFromLead:(MMSF_Object *)leadToSave {
    originalLeadProperties =  [NSDictionary dictionaryWithObjectsAndKeys:
                               [leadToSave valueForKey:@"FirstName"], @"FirstName",
                               [leadToSave valueForKey:@"LastName"], @"LastName",
                               [leadToSave valueForKey:@"Phone"], @"Phone",
                               [leadToSave valueForKey:@"Company"], @"Company",
                               [leadToSave valueForKey:@"Email"], @"Email",
                               [leadToSave valueForKey:@"Street"], @"Street",
                               [leadToSave valueForKey:@"City"], @"City",
                               [leadToSave valueForKey:@"State"], @"State",
                               [leadToSave valueForKey:@"PostalCode"], @"PostalCode",
                               [leadToSave valueForKey:@"Country"], @"Country",
                               nil];
}

- (void) restorePropertiesIntoLead:(MMSF_Object *)leadToRestore {
    if (originalLeadProperties != nil) {
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"FirstName"] forKey:@"FirstName"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"LastName"] forKey:@"LastName"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Phone"] forKey:@"Phone"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Company"] forKey:@"Company"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Email"] forKey:@"Email"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Street"] forKey:@"Street"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"City"] forKey:@"City"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"State"] forKey:@"State"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"PostalCode"] forKey:@"PostalCode"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Country"] forKey:@"Country"];
    }
}

#pragma mark - Button Actions
- (IBAction)leadEditingSwitch:(id)sender {
    if ([[editButton titleForState:UIControlStateNormal] isEqualToString:@"Edit"]) {
        [editButton setTitle:@"Done" forState:UIControlStateNormal];
        [cancelButton setAlpha:1.0];
        [backButton setAlpha:0.0];
        
        [firstNameTextField setBorderStyle:UITextBorderStyleLine];
        [lastNameTextField  setBorderStyle:UITextBorderStyleLine];
        [contactNumberTextField  setBorderStyle:UITextBorderStyleLine];
        [companyTextfield  setBorderStyle:UITextBorderStyleLine];
        [mailTextField  setBorderStyle:UITextBorderStyleLine];
        [streetTextField  setBorderStyle:UITextBorderStyleLine];
        [cityTextField  setBorderStyle:UITextBorderStyleLine];
        [stateTextField  setBorderStyle:UITextBorderStyleLine];
        [postalCodeTextField  setBorderStyle:UITextBorderStyleLine];
        [countryTextField  setBorderStyle:UITextBorderStyleLine];
        
        [firstNameTextField becomeFirstResponder];
        
        [firstNameTextField setUserInteractionEnabled:YES];
        [lastNameTextField  setUserInteractionEnabled:YES];
        [contactNumberTextField  setUserInteractionEnabled:YES];
        [companyTextfield  setUserInteractionEnabled:YES];
        [mailTextField  setUserInteractionEnabled:YES];
        [streetTextField  setUserInteractionEnabled:YES];
        [cityTextField  setUserInteractionEnabled:YES];
        [stateTextField  setUserInteractionEnabled:YES];
        [postalCodeTextField  setUserInteractionEnabled:YES];
        [countryTextField  setUserInteractionEnabled:YES];
        
    } else {
        if ([self validateDataFields]) {
            [cancelButton setAlpha:0.0];
            [backButton setAlpha:1.0];
            [editButton setTitle:@"Edit" forState:UIControlStateNormal];
        }
    }
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelButtonAction:(id)sender {
    [cancelButton setAlpha:0.0];
    [backButton setAlpha:1.0];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    
    [firstNameTextField setBorderStyle:UITextBorderStyleNone];
    [lastNameTextField  setBorderStyle:UITextBorderStyleNone];
    [contactNumberTextField  setBorderStyle:UITextBorderStyleNone];
    [companyTextfield  setBorderStyle:UITextBorderStyleNone];
    [mailTextField  setBorderStyle:UITextBorderStyleNone];
    [streetTextField  setBorderStyle:UITextBorderStyleNone];
    [cityTextField  setBorderStyle:UITextBorderStyleNone];
    [stateTextField  setBorderStyle:UITextBorderStyleNone];
    [postalCodeTextField  setBorderStyle:UITextBorderStyleNone];
    [countryTextField  setBorderStyle:UITextBorderStyleNone];
    
    [firstNameTextField setUserInteractionEnabled:NO];
    [lastNameTextField  setUserInteractionEnabled:NO];
    [contactNumberTextField  setUserInteractionEnabled:NO];
    [companyTextfield  setUserInteractionEnabled:NO];
    [mailTextField  setUserInteractionEnabled:NO];
    [streetTextField  setUserInteractionEnabled:NO];
    [cityTextField  setUserInteractionEnabled:NO];
    [stateTextField  setUserInteractionEnabled:NO];
    [postalCodeTextField  setUserInteractionEnabled:NO];
    [countryTextField  setUserInteractionEnabled:NO];
    
    [firstNameTextField setOpaque:NO];
}

#pragma mark - Validation
- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

- (BOOL) validatePhoneNumber:(NSString *)phoneNumber {
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                               error:nil];
    NSUInteger numberOfMatches = [detector numberOfMatchesInString:phoneNumber
                                                           options:0
                                                             range:NSMakeRange(0, [phoneNumber length])];
    if (numberOfMatches == 0) {
        return NO;
    }
    return YES;
}

- (BOOL) validateDataFields {
    
    if (![[firstNameTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No First Name!" message:@"First Name should not be empty!!"];
        [firstNameTextField becomeFirstResponder];
        return NO;
    }
    if (![[lastNameTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Last Name!" message:@"Last Name should not be empty!!"];
        [lastNameTextField becomeFirstResponder];
        return NO;
    }
    if (![[companyTextfield text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Company Name!" message:@"Company Name should not be empty!!"];
        [companyTextfield becomeFirstResponder];
        return NO;
    }
    if (![[contactNumberTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Contact Number!" message:@"Contact Number should not be empty!!"];
        [contactNumberTextField becomeFirstResponder];
        return NO;
    }
    if (![self validatePhoneNumber:[contactNumberTextField text]]) {
        [SA_AlertView showAlertWithTitle: @"Please enter a valid Contact Number" message: @""];
        [contactNumberTextField becomeFirstResponder];
        return NO;
    }
    if (![[mailTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Mail Id!" message:@"Mail Id should not be empty!!"];
        [mailTextField becomeFirstResponder];
        return NO;
    }
    if (![self validateEmail:[mailTextField text]]) {
        [SA_AlertView showAlertWithTitle:@"Invalid Email!" message:@"Email should be valid!!"];
        [contactNumberTextField becomeFirstResponder];
        return NO;
    }
    
    [firstNameTextField setBorderStyle:UITextBorderStyleNone];
    [lastNameTextField  setBorderStyle:UITextBorderStyleNone];
    [contactNumberTextField  setBorderStyle:UITextBorderStyleNone];
    [companyTextfield  setBorderStyle:UITextBorderStyleNone];
    [mailTextField  setBorderStyle:UITextBorderStyleNone];
    [streetTextField  setBorderStyle:UITextBorderStyleNone];
    [cityTextField  setBorderStyle:UITextBorderStyleNone];
    [stateTextField  setBorderStyle:UITextBorderStyleNone];
    [postalCodeTextField  setBorderStyle:UITextBorderStyleNone];
    [countryTextField  setBorderStyle:UITextBorderStyleNone];
    
    [firstNameTextField setUserInteractionEnabled:NO];
    [lastNameTextField  setUserInteractionEnabled:NO];
    [contactNumberTextField  setUserInteractionEnabled:NO];
    [companyTextfield  setUserInteractionEnabled:NO];
    [mailTextField  setUserInteractionEnabled:NO];
    [streetTextField  setUserInteractionEnabled:NO];
    [cityTextField  setUserInteractionEnabled:NO];
    [stateTextField  setUserInteractionEnabled:NO];
    [postalCodeTextField  setUserInteractionEnabled:NO];
    [countryTextField  setUserInteractionEnabled:NO];
    
    [editedLead setValue:firstNameTextField.text forKey:@"FirstName"];
    [editedLead setValue:lastNameTextField.text forKey:@"LastName"];
    [editedLead setValue:contactNumberTextField.text forKey:@"Phone"];
    [editedLead setValue:companyTextfield.text forKey:@"Company"];
    [editedLead setValue:mailTextField.text forKey:@"Email"];
    [editedLead setValue:streetTextField.text forKey:@"Street"];
    [editedLead setValue:cityTextField.text forKey:@"City"];
    [editedLead setValue:stateTextField.text forKey:@"State"];
    [editedLead setValue:postalCodeTextField.text forKey:@"PostalCode"];
    [editedLead setValue:countryTextField.text forKey:@"Country"];
    [editedLead setValue:[NSDate date] forKey:@"LastModifiedDate"];
    
    [editedLead finishEditingSavingChanges:YES];
    return YES;
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if(theTextField == firstNameTextField) {
        [lastNameTextField becomeFirstResponder];
    } else if(theTextField == lastNameTextField) {
        [contactNumberTextField becomeFirstResponder];
    } else if(theTextField == contactNumberTextField) {
        [companyTextfield becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 120.0) animated:YES];
    } else if(theTextField == companyTextfield){
        [mailTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 160.0) animated:YES];
    } else if(theTextField == mailTextField) {
        [streetTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 200.0) animated:YES];
    } else if(theTextField == streetTextField) {
        [cityTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 240.0) animated:YES];
    } else if(theTextField == cityTextField) {
        [stateTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 280.0) animated:YES];
    } else if(theTextField == stateTextField) {
        [postalCodeTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 320.0) animated:YES];
    } else if(theTextField == postalCodeTextField) {
        [countryTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 360.0) animated:YES];
    } else {
        [countryTextField resignFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}


@end
