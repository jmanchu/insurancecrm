//
//  SFDCLeadDetailsViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCLeadDetailsViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UITextField *firstNameTextField;
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *contactNumberTextField;
    IBOutlet UITextField *companyTextfield;
    IBOutlet UITextField *mailTextField;
    IBOutlet UITextField *streetTextField;
    IBOutlet UITextField *cityTextField;
    IBOutlet UITextField *stateTextField;
    IBOutlet UITextField *postalCodeTextField;
    IBOutlet UITextField *countryTextField;
    IBOutlet UILabel *titleLabel;
    
    IBOutlet UIScrollView *aScrollView;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *editButton;
}
- (IBAction)leadEditingSwitch:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
+ (SFDCLeadDetailsViewController *)createControllerForParent:(id)parentController usingLead:(MMSF_Object *)leadObj;
@end
