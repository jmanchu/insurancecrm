//
//  main.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 12/24/12.
//  Copyright (c) 2012 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SFDCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SFDCAppDelegate class]));
    }
}
