//
//  SFDCProductsViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCProductsViewController.h"
#import "SFDCProductDetailsViewController.h"
#import "SFDCAppDelegate.h"

@interface SFDCProductsViewController () {
    
}
@property (nonatomic, retain) MMSF_Object *currentUser;
@property (nonatomic, strong) NSFetchedResultsController *fetchedSearchResultsController;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *context;


@end

@implementation SFDCProductsViewController
@synthesize currentUser, fetchedSearchResultsController, fetchedResultsController, context;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
     * Fetch on fetchResultsController.
     */
    NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
		 */
		//NSLog(@"Unresolved fetchedResultsController error %@, %@", error, [error userInfo]);
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Fetch Products
- (NSFetchedResultsController *)fetchedResultsController {
    [NSFetchedResultsController deleteCacheWithName:nil];
	if (fetchedResultsController != nil) {
		return fetchedResultsController;
	}
    
	// Create and configure a fetch request with the Task entity.
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    self.context = [[MM_ContextManager sharedManager] contentContextForWriting];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Product__c" inManagedObjectContext:self.context];
	[fetchRequest setEntity:entity];
    
    // Create the predicate for ownerId & status
    self.currentUser = [MM_SyncManager currentUserInContext:self.context];
//    [fetchRequest setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:$A($P(@"OwnerId == %@",[self.currentUser valueForKey:@"Id"]), nil)]];
    
	// Create the sort descriptors array.
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
    
	// Create and initialize the fetch results controller.
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.context
                                                                                                  sectionNameKeyPath:@"Type__c"
                                                                                                           cacheName:@"Product__c"];
    
	self.fetchedResultsController = aFetchedResultsController;
	fetchedResultsController.delegate = self;
    
	return fetchedResultsController;
}

#pragma mark - Reload data after each refresh
-(void)reloadProductsViewWithNewData {
    [NSFetchedResultsController deleteCacheWithName:nil];
    self.fetchedResultsController = nil;
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
                 Replace this implementation with code to handle the error appropriately.
         
                 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
                 */
        NSLog(@"Unresolved fetchedResultsController error %@, %@", error, [error userInfo]);
    }
    NSLog(@"Total Leads: %d",[fetchedResultsController.fetchedObjects count]);
    [aTableView reloadData];
}


#pragma mark - Button Actions
- (IBAction)refreshButtonAction:(id)sender {
    SFDCAppDelegate *appDel = (SFDCAppDelegate *)([[UIApplication sharedApplication]delegate]);
    [appDel refreshAction];
}

- (IBAction)backButtonAction:(id)sender {
    [UIView animateWithDuration:0.75 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view setAlpha:0.0];
    } completion:^(BOOL finished) {
        [self.navigationController popViewControllerAnimated:NO];
    }];
}

#pragma mark - UITableViewDelegate
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    /*
     * Load details.
     */
//    if ([[fetchedResultsController sections] count] > 0) {
//        MMSF_Object *productObj = (MMSF_Object *)[fetchedResultsController objectAtIndexPath:indexPath];
//        SFDCProductDetailsViewController *productDetailsViewController = [[SFDCProductDetailsViewController alloc] initWithNibName:@"SFDCProductDetailsViewController" bundle:nil];
//        [self.navigationController pushViewController:productDetailsViewController animated:YES];
//    }
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (![[fetchedResultsController sections] count]) {
        return @"No Products!!";
    }
    id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo name];
}

- (UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath: (NSIndexPath *) indexPath {
    static NSString *noLeadIdentifier = @"NoLeadIdentifier";
    static NSString *leadIdentifier = @"LeadIdentifier";
    
    if (![[fetchedResultsController sections] count] > 0) {
        UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:noLeadIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noLeadIdentifier];
        }
        cell.textLabel.text = [NSString stringWithFormat:@"No Products Found!"];//@"No Tasks Found"];
        cell.textLabel.textColor = [UIColor orangeColor];
        return cell;
    } else {
        UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:leadIdentifier];
		if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:leadIdentifier];
        }
        MMSF_Object *productObj = (MMSF_Object *)[fetchedResultsController objectAtIndexPath:indexPath];
        cell.textLabel.text = [productObj valueForKey:@"Name"];
        return cell;
    }
}

- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView {
    NSInteger count = [[fetchedResultsController sections] count];
	if (!count) {
		count = 1;
	}
    return count;
}

- (NSInteger) tableView: (UITableView *) tableView numberOfRowsInSection: (NSInteger) section {
    NSInteger numberOfRows = 0;
	
    if ([[fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    } else {
        numberOfRows = 1;
    }
    
    return numberOfRows;
}

@end
