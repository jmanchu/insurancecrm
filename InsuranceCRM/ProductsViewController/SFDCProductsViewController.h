//
//  SFDCProductsViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCProductsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate> {
    IBOutlet UITableView *aTableView;
}
- (IBAction)refreshButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;
/*
 * Call after each refresh sync
 */
-(void)reloadProductsViewWithNewData;

@end
