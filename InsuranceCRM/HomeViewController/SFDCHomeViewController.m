//
//  SFDCHomeViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 12/31/12.
//  Copyright (c) 2012 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCHomeViewController.h"
#import "SFDCLeadsViewController.h"
#import "SFDCAccountsViewController.h"
#import "SFDCProductsViewController.h"
#import "SFDCCreatePolicyViewController.h"
#import "SFDCPremiumCalViewController.h"
#import "SFDCAppDelegate.h"

@interface SFDCHomeViewController ()

@end

@implementation SFDCHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:leftPaneView];
    [self.view addSubview:rightPaneView];
    
    /*
     * Hide left pane.
     */
    CGRect leftPaneFrame = leftPaneView.frame;
    leftPaneFrame.origin.x = - leftPaneFrame.size.width;
    leftPaneFrame.origin.y = 50;
    leftPaneView.frame = leftPaneFrame;
    
    /*
     * Hide right pane.
     */
    CGRect rightPaneFrame = rightPaneView.frame;
    rightPaneFrame.origin.x = rightPaneFrame.size.width;
    rightPaneFrame.origin.y = 50;
    rightPaneView.frame = rightPaneFrame;
    
    [self.view addSubview:accountsButton];
    [self.view addSubview:productsButton];
    [self.view addSubview:leadButton];
    [self.view addSubview:policyButton];
    [self.view addSubview:premiumCalButton];
    
    CGRect frame = accountsButton.frame;
    frame.origin.x = 121;
    frame.origin.y = 156;
    accountsButton.frame = frame;
    
    frame = productsButton.frame;
    frame.origin.x = 132;
    frame.origin.y = 323;
    productsButton.frame = frame;

    frame = leadButton.frame;
    frame.origin.x = 39;
    frame.origin.y = 73;
    leadButton.frame = frame;
    
    frame = policyButton.frame;
    frame.origin.x = 39;
    frame.origin.y = 235;
    policyButton.frame = frame;
    
    frame = premiumCalButton.frame;
    frame.origin.x = 39;
    frame.origin.y = 397;
    premiumCalButton.frame = frame;
    
    [accountsButton setAlpha:0.0];
    [productsButton setAlpha:0.0];
    [leadButton setAlpha:0.0];
    [policyButton setAlpha:0.0];
    [premiumCalButton setAlpha:0.0];
    
    
//    [rightPaneView setUserInteractionEnabled:NO];
//    for (id object in [rightPaneView subviews]) {
//        if ([object isKindOfClass:[UIButton class]]) {
//            [object setUserInteractionEnabled:YES];
//        } else if ([object isKindOfClass:[UIImageView class]]) {
//            [object setUserInteractionEnabled:NO];
//        }
//        else {
//            [object setUserInteractionEnabled:NO];
//        }
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    /*
     * Animate & present the left & right panes.
     */
    CGRect leftPaneFrame = leftPaneView.frame;
    leftPaneFrame.origin.x = 0;
    
    CGRect rightPaneFrame = rightPaneView.frame;
    rightPaneFrame.origin.x = 0;
    [self.view setAlpha:1.0];
    
    float delay = 0.0;
    [UIView animateWithDuration:0.75 delay:delay options:UIViewAnimationCurveEaseIn animations:^{
        [leftPaneView setFrame:leftPaneFrame];
        [rightPaneView setFrame:rightPaneFrame];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.0 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
            
        } completion:^(BOOL finished) {
            [accountsButton setAlpha:1.0];
            [productsButton setAlpha:1.0];
            [leadButton setAlpha:1.0];
            [policyButton setAlpha:1.0];
            [premiumCalButton setAlpha:1.0];
            
            [accountsImgView setAlpha:0.0];
            [productsImgView setAlpha:0.0];
            [leadImgView setAlpha:0.0];
            [policyImgView setAlpha:0.0];
            [premiumCalImgView setAlpha:0.0];
        }];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)animateScreenAndPush:(UIViewController *)viewController {
    /*
     * Animate & dismiss the left & right panes.
     */
    [viewController.view setAlpha:0.0];
    
    [accountsButton setAlpha:0.0];
    [productsButton setAlpha:0.0];
    [leadButton setAlpha:0.0];
    [policyButton setAlpha:0.0];
    [premiumCalButton setAlpha:0.0];
    
    [accountsImgView setAlpha:1.0];
    [productsImgView setAlpha:1.0];
    [leadImgView setAlpha:1.0];
    [policyImgView setAlpha:1.0];
    [premiumCalImgView setAlpha:1.0];
    
    CGRect leftPaneFrame = leftPaneView.frame;
    leftPaneFrame.origin.x = - leftPaneFrame.size.width;
    CGRect rightPaneFrame = rightPaneView.frame;
    rightPaneFrame.origin.x = rightPaneFrame.size.width;
    float delay = 0.0;
    [UIView animateWithDuration:0.75 delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
        [leftPaneView setFrame:leftPaneFrame];
        [rightPaneView setFrame:rightPaneFrame];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.75 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.navigationController pushViewController:viewController animated:NO];
            [viewController.view setAlpha:1.0];
        } completion:NULL];
    }];
}

#pragma mark - Button Actions

- (IBAction)leadSelected:(id)sender {
    SFDCLeadsViewController *leadsViewController = [[SFDCLeadsViewController alloc] initWithNibName:@"SFDCLeadsViewController" bundle:nil];
    [self animateScreenAndPush:leadsViewController];
}

- (IBAction)accountsSelected:(id)sender {
    SFDCAccountsViewController *accountsViewController = [[SFDCAccountsViewController alloc] initWithNibName:@"SFDCAccountsViewController" bundle:nil];
    [self animateScreenAndPush:accountsViewController];
}

- (IBAction)policySelected:(id)sender {
    SFDCCreatePolicyViewController *createPolicyViewController = [SFDCCreatePolicyViewController createPolicyViewControllerFrom:self];
    [self animateScreenAndPush:createPolicyViewController];
}

- (IBAction)premiumCalculatorSelected:(id)sender {
    SFDCPremiumCalViewController *premiumCalViewController = [SFDCPremiumCalViewController createPremiumCalViewControllerFrom:self];
    [self animateScreenAndPush:premiumCalViewController];
}

- (IBAction)productsSelected:(id)sender {
    SFDCProductsViewController *productsViewController = [[SFDCProductsViewController alloc] initWithNibName:@"SFDCProductsViewController" bundle:nil];
    [self animateScreenAndPush:productsViewController];
}

- (IBAction)settingsButtonAction:(id)sender {
    
}

- (IBAction)refreshButtonAction:(id)sender {
    SFDCAppDelegate *appDel = (SFDCAppDelegate *)([[UIApplication sharedApplication]delegate]);
    [appDel refreshAction];
}

@end
