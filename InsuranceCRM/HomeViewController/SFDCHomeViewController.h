//
//  SFDCHomeViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 12/31/12.
//  Copyright (c) 2012 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCHomeViewController : UIViewController {
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIView *leftPaneView;
    IBOutlet UIView *rightPaneView;
    
    IBOutlet UIButton *accountsButton;
    IBOutlet UIButton *productsButton;
    IBOutlet UIButton *policyButton;
    IBOutlet UIButton *leadButton;
    IBOutlet UIButton *premiumCalButton;
    
    IBOutlet UIImageView *accountsImgView;
    IBOutlet UIImageView *productsImgView;
    IBOutlet UIImageView *policyImgView;
    IBOutlet UIImageView *leadImgView;
    IBOutlet UIImageView *premiumCalImgView;
}

- (IBAction)leadSelected:(id)sender;
- (IBAction)accountsSelected:(id)sender;
- (IBAction)policySelected:(id)sender;
- (IBAction)premiumCalculatorSelected:(id)sender;
- (IBAction)productsSelected:(id)sender;

- (IBAction)settingsButtonAction:(id)sender;
- (IBAction)refreshButtonAction:(id)sender;

@end
