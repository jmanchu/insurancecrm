//
//  SFDCLoginViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 12/31/12.
//  Copyright (c) 2012 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCLoginViewController.h"

@interface SFDCLoginViewController ()
@property (nonatomic, strong) MM_LoginViewController *loginViewController;

@end

@implementation SFDCLoginViewController
@synthesize loginViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadLoginView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [statusLabel setText:@""];
}

- (void)loadLoginView {
    if ([UIDevice connectionType] != connection_none) {
        self.loginViewController = [MM_LoginViewController presentModallyInParent:self];
        [[loginViewController navigationBar] setHidden:YES];
        loginViewController.useTestServer = NO;
        loginViewController.forceLoginOnFreshInstall = YES;
        loginViewController.canToggleServer = NO;
        loginViewController.canCancel = NO;
        [loginViewController.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        loginViewController.navigationBar.topItem.leftBarButtonItem = nil;
        [[loginViewController serverToggleSwitch] setOn:NO];
    } else {
        /*
         * Through an alert saying to connect to internet.
         */
        [statusLabel setText:@"No Internety Connectivity!!"];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)appWillEnterForeground:(id)sender {
    if (!self.loginViewController) {
        [self loadLoginView];
    }
}

@end
