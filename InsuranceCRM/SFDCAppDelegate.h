//
//  SFDCAppDelegate.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 12/24/12.
//  Copyright (c) 2012 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SFDCHomeViewController;
@class SFDCHomeViewController_iPad;
@class SFDCLoginViewController;

@interface SFDCAppDelegate : UIResponder <UIApplicationDelegate> {
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SFDCLoginViewController *loginViewController;
@property (strong, nonatomic) id homeViewController;
@property (strong, nonatomic) UINavigationController *navigationController;

- (void)refreshAction;

@end
