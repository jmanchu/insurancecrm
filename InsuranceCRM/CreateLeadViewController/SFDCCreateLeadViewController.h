//
//  SFDCCreateLeadViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCCreateLeadViewController : UIViewController <UITextFieldDelegate> {
    
    IBOutlet UITextField *firstNameTextField;
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *contactNumberTextField;
    IBOutlet UITextField *companyTextfield;
    IBOutlet UITextField *mailTextField;
    IBOutlet UITextField *streetTextField;
    IBOutlet UITextField *cityTextField;
    IBOutlet UITextField *stateTextField;
    IBOutlet UITextField *postalCodeTextField;
    IBOutlet UITextField *countryTextField;
    IBOutlet UILabel *titleLabel;
    
    IBOutlet UIScrollView *aScrollView;
    
    NSManagedObjectContext *context;
}
@property (nonatomic, strong) NSManagedObjectContext *context;
- (IBAction)leadCreationDone:(id)sender;
- (IBAction) cancel: (id) sender;

- (void)makeFirstNameFirstResponder;

+ (SFDCCreateLeadViewController *)createControllerForParent:(id)parentController byEditingLead:(MMSF_Object *)leadObj;
+ (SFDCCreateLeadViewController *)createControllerForParent:(id)parentController;

@end