//
//  SFDCCreateLeadViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCCreateLeadViewController.h"
#import "SFDCLeadsViewController.h"

static MMSF_Object *editedLead;
static SFDCLeadsViewController *leadsViewController;

@interface SFDCCreateLeadViewController () {
    MMSF_Object *newLead;
}
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) NSDictionary *originalLeadProperties;
- (void)saveStringPropertiesFromLead:(MMSF_Object *)leadToSave;
- (void)restorePropertiesIntoLead:(MMSF_Object *)leadToRestore;

@end

@implementation SFDCCreateLeadViewController
@synthesize titleLabel, originalLeadProperties, context;

+ (SFDCCreateLeadViewController *)createControllerForParent:(id)parentController byEditingLead:(MMSF_Object *)leadObj {
    editedLead = leadObj;
    NSString *nibName = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nibName = @"SFDCCreateLeadViewController_iPad";
    } else {
        nibName = @"SFDCCreateLeadViewController";
    }
    SFDCCreateLeadViewController *editLeadViewController = [[SFDCCreateLeadViewController alloc] initWithNibName:nibName bundle:nil];
    leadsViewController = parentController;
    [editLeadViewController.titleLabel setText:@"Edit Lead"];
    editLeadViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    return editLeadViewController;
}

+ (SFDCCreateLeadViewController *)createControllerForParent:(id)parentController {
    editedLead = nil;
    NSString *nibName = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nibName = @"SFDCCreateLeadViewController_iPad";
    } else {
        nibName = @"SFDCCreateLeadViewController";
    }
    SFDCCreateLeadViewController *addLeadViewController = [[SFDCCreateLeadViewController alloc] initWithNibName:nibName bundle:nil];
    leadsViewController = parentController;
    [addLeadViewController.titleLabel setText:@"Add Lead"];
    addLeadViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    return addLeadViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.superview.frame = self.view.frame;//it's important to do this after presentModalViewController
    self.view.superview.center = self.view.center;
    
    // Do any additional setup after loading the view from its nib.
    [aScrollView setContentSize:CGSizeMake(320, 520)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(canProceedWithSaving:) name: kNotification_ObjectChangesSaved object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(canProceedWithSaving:) name: kNotification_ObjectCreated object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(objectSavingFailed:) name: kNotification_ObjectSaveError object: nil];
    
    if (editedLead != nil) {
        self.originalLeadProperties = nil;
        [self saveStringPropertiesFromLead:editedLead];
        
        [firstNameTextField setText:[editedLead valueForKey:@"FirstName"]];
        [lastNameTextField setText:[editedLead valueForKey:@"LastName"]];
        [contactNumberTextField setText:[editedLead valueForKey:@"Phone"]];
        [companyTextfield setText:[editedLead valueForKey:@"Company"]];
        [mailTextField setText:[editedLead valueForKey:@"Email"]];
        [streetTextField setText:[editedLead valueForKey:@"Street"]];
        [cityTextField setText:[editedLead valueForKey:@"City"]];
        [stateTextField setText:[editedLead valueForKey:@"State"]];
        [postalCodeTextField setText:[editedLead valueForKey:@"PostalCode"]];
        [countryTextField setText:[editedLead valueForKey:@"Country"]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    lastNameTextField = nil;
    firstNameTextField = nil;
    contactNumberTextField = nil;
    companyTextfield = nil;
    mailTextField = nil;
    streetTextField = nil;
    cityTextField = nil;
    stateTextField = nil;
    postalCodeTextField = nil;
    countryTextField = nil;
    
    aScrollView = nil;
    [super viewDidUnload];
}

#pragma mark - Notifications
- (void)dismissModalViewWithAnimation {
    if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self dismissModalViewControllerAnimated:YES];
    } else if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (void) canProceedWithSaving: (NSNotification *) note {
    originalLeadProperties = nil;
    editedLead = nil;
    [self dismissModalViewWithAnimation];
}

- (void) objectSavingFailed: (NSNotification *) note {
    if (newLead != nil) {
        [newLead deleteFromContext];
        newLead = nil;
    } else {
        [self restorePropertiesIntoLead:editedLead];
        [editedLead save];
    }
}


#pragma mark - Button Actions
- (void) restorePropertiesIntoLead:(MMSF_Object *)leadToRestore {
    if (originalLeadProperties != nil) {
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"FirstName"] forKey:@"FirstName"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"LastName"] forKey:@"LastName"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Phone"] forKey:@"Phone"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Company"] forKey:@"Company"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Email"] forKey:@"Email"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Street"] forKey:@"Street"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"City"] forKey:@"City"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"State"] forKey:@"State"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"PostalCode"] forKey:@"PostalCode"];
        [leadToRestore setValue:[originalLeadProperties objectForKey:@"Country"] forKey:@"Country"];
    }
}


- (void) saveStringPropertiesFromLead: (MMSF_Object *)leadToSave {
    
    originalLeadProperties =  [NSDictionary dictionaryWithObjectsAndKeys:
                                  [leadToSave valueForKey:@"FirstName"], @"FirstName",
                                  [leadToSave valueForKey:@"LastName"], @"LastName",
                                  [leadToSave valueForKey:@"Phone"], @"Phone",
                                  [leadToSave valueForKey:@"Company"], @"Company",
                                  [leadToSave valueForKey:@"Email"], @"Email",
                                  [leadToSave valueForKey:@"Street"], @"Street",
                                  [leadToSave valueForKey:@"City"], @"City",
                                  [leadToSave valueForKey:@"State"], @"State",
                                  [leadToSave valueForKey:@"PostalCode"], @"PostalCode",
                                  [leadToSave valueForKey:@"Country"], @"Country",
                                  nil];
}


- (IBAction)leadCreationDone:(id)sender {
    [self validateDataFields];
}

- (IBAction) cancel: (id) sender {
    self.originalLeadProperties = nil;
	[self dismissModalViewWithAnimation];
}


- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

- (BOOL) validatePhoneNumber:(NSString *)phoneNumber {
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                               error:nil];
    NSUInteger numberOfMatches = [detector numberOfMatchesInString:phoneNumber
                                                           options:0
                                                             range:NSMakeRange(0, [phoneNumber length])];
    if (numberOfMatches == 0) {
        return NO;
    }
    return YES;
}

- (void)validateDataFields {
    
    if (![[firstNameTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No First Name!" message:@"First Name should not be empty!!"];
        [firstNameTextField becomeFirstResponder];
        return;
    }
    if (![[lastNameTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Last Name!" message:@"Last Name should not be empty!!"];
        [lastNameTextField becomeFirstResponder];
        return;
    }
    if (![[companyTextfield text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Company Name!" message:@"Company Name should not be empty!!"];
        [companyTextfield becomeFirstResponder];
        return;
    }
    if (![[contactNumberTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Contact Number!" message:@"Contact Number should not be empty!!"];
        [contactNumberTextField becomeFirstResponder];
        return;
    }
    if (![self validatePhoneNumber:[contactNumberTextField text]]) {
        [SA_AlertView showAlertWithTitle: @"Please enter a valid Contact Number" message: @""];
        [contactNumberTextField becomeFirstResponder];
        return;
    }
    if (![[mailTextField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Mail Id!" message:@"Mail Id should not be empty!!"];
        [mailTextField becomeFirstResponder];
        return;
    }
    if (![self validateEmail:[mailTextField text]]) {
        [SA_AlertView showAlertWithTitle:@"Invalid Email!" message:@"Email should be valid!!"];
        [contactNumberTextField becomeFirstResponder];
        return;
    }
    if (!editedLead) {
//        NSManagedObjectContext * ctx = [[MM_ContextManager sharedManager] mainContentContext];
        newLead = (MMSF_Object*)[self.context insertNewEntityWithName:@"Lead"];
        [newLead beginEditing];
        
        [newLead setValue:firstNameTextField.text forKey:@"FirstName"];
        [newLead setValue:lastNameTextField.text forKey:@"LastName"];
        [newLead setValue:contactNumberTextField.text forKey:@"Phone"];
        [newLead setValue:companyTextfield.text forKey:@"Company"];
        [newLead setValue:mailTextField.text forKey:@"Email"];
        [newLead setValue:streetTextField.text forKey:@"Street"];
        [newLead setValue:cityTextField.text forKey:@"City"];
        [newLead setValue:stateTextField.text forKey:@"State"];
        [newLead setValue:postalCodeTextField.text forKey:@"PostalCode"];
        [newLead setValue:countryTextField.text forKey:@"Country"];
        
        [newLead finishEditingSavingChanges:YES];
    } else {
        [editedLead setValue:firstNameTextField.text forKey:@"FirstName"];
        [editedLead setValue:lastNameTextField.text forKey:@"LastName"];
        [editedLead setValue:contactNumberTextField.text forKey:@"Phone"];
        [editedLead setValue:companyTextfield.text forKey:@"Company"];
        [editedLead setValue:mailTextField.text forKey:@"Email"];
        [editedLead setValue:streetTextField.text forKey:@"Street"];
        [editedLead setValue:cityTextField.text forKey:@"City"];
        [editedLead setValue:stateTextField.text forKey:@"State"];
        [editedLead setValue:postalCodeTextField.text forKey:@"PostalCode"];
        [editedLead setValue:countryTextField.text forKey:@"Country"];
        [editedLead setValue:[NSDate date] forKey:@"LastModifiedDate"];
        
        [editedLead finishEditingSavingChanges:YES];
    }
}

- (void)makeFirstNameFirstResponder {
    [firstNameTextField becomeFirstResponder];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if(theTextField == firstNameTextField) {
        [lastNameTextField becomeFirstResponder];
    } else if(theTextField == lastNameTextField) {
        [contactNumberTextField becomeFirstResponder];
    } else if(theTextField == contactNumberTextField) {
        [companyTextfield becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 120.0) animated:YES];
    } else if(theTextField == companyTextfield){
        [mailTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 160.0) animated:YES];
    } else if(theTextField == mailTextField) {
        [streetTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 200.0) animated:YES];
    } else if(theTextField == streetTextField) {
        [cityTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 240.0) animated:YES];
    } else if(theTextField == cityTextField) {
        [stateTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 280.0) animated:YES];
    } else if(theTextField == stateTextField) {
        [postalCodeTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 320.0) animated:YES];
    } else if(theTextField == postalCodeTextField) {
        [countryTextField becomeFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 360.0) animated:YES];
    } else {
        [countryTextField resignFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}

@end
