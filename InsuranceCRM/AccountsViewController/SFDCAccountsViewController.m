//
//  SFDCAccountsViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCAccountsViewController.h"
#import "SFDCAccountDetailsViewController.h"
#import "SFDCAppDelegate.h"
#import "CustomCell.h"
@interface SFDCAccountsViewController () {
    
}
@property (nonatomic, retain) MMSF_Object *currentUser;
@property (nonatomic, strong) NSFetchedResultsController *fetchedSearchResultsController;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *context;


@end

@implementation SFDCAccountsViewController
@synthesize currentUser, fetchedSearchResultsController, fetchedResultsController, context;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
     * Fetch on fetchResultsController.
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncEnded:) name:kNotification_SyncComplete object:nil];
    
    NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
		 */
		//NSLog(@"Unresolved fetchedResultsController error %@, %@", error, [error userInfo]);
	}
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadAccountsViewWithNewData];
}

- (void)viewWillAppear:(BOOL)animated {
//    [self reloadAccountsViewWithNewData];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotification_SyncComplete object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Fetch Leads
- (NSFetchedResultsController *)fetchedResultsController {
    [NSFetchedResultsController deleteCacheWithName:nil];
	if (fetchedResultsController != nil) {
		return fetchedResultsController;
	}
    
	// Create and configure a fetch request with the Task entity.
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    self.context = [[MM_ContextManager sharedManager] contentContextForWriting];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Account" inManagedObjectContext:self.context];
	[fetchRequest setEntity:entity];
    
    // Create the predicate for ownerId & status
    self.currentUser = [MM_SyncManager currentUserInContext:self.context];
    [fetchRequest setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:$A($P(@"OwnerId == %@",self.currentUser), nil)]];
    
	// Create the sort descriptors array.
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
    
	// Create and initialize the fetch results controller.
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.context
                                                                                                  sectionNameKeyPath:@"LastModifiedDate"
                                                                                                           cacheName:@"Account"];
    
	self.fetchedResultsController = aFetchedResultsController;
	fetchedResultsController.delegate = self;
    
	return fetchedResultsController;
}

#pragma mark - Reload data after each refresh
- (void)syncEnded:(NSNotification*)notification {
    /*
     * Hide activity indictor.
     */
    [self reloadAccountsViewWithNewData];
}

-(void)reloadAccountsViewWithNewData
{
    [NSFetchedResultsController deleteCacheWithName:nil];
    self.fetchedResultsController = nil;
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        /*
                 Replace this implementation with code to handle the error appropriately.
         
                 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
                 */
        NSLog(@"Unresolved fetchedResultsController error %@, %@", error, [error userInfo]);
    }
    NSLog(@"Total Leads: %d",[fetchedResultsController.fetchedObjects count]);
    [aTableView reloadData];
}


#pragma mark - Button Actions
- (IBAction)refreshButtonAction:(id)sender {
    SFDCAppDelegate *appDel = (SFDCAppDelegate *)([[UIApplication sharedApplication]delegate]);
    [appDel refreshAction];

}

- (IBAction)backButtonAction:(id)sender {
    [UIView animateWithDuration:0.75 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view setAlpha:0.0];
    } completion:^(BOOL finished) {
        [self.navigationController popViewControllerAnimated:NO];
    }];
}

- (IBAction)createAccount:(id)sender {
    
}

#pragma mark - UITableViewDelegate
- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MMSF_Object *accountObj = (MMSF_Object *)[[fetchedResultsController fetchedObjects] objectAtIndex:indexPath.row];
    SFDCAccountDetailsViewController *accountDetailsViewController = [SFDCAccountDetailsViewController accountDetailsControllerForParent:self withAccount:accountObj];
    [self.navigationController pushViewController:accountDetailsViewController animated:YES];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath: (NSIndexPath *) indexPath {
    static NSString *noLeadIdentifier = @"NoLeadIdentifier";
    static NSString *leadIdentifier = @"LeadIdentifier";
    
    if (![[fetchedResultsController sections] count] > 0) {
        CustomCell *cell=(CustomCell *)[tableView dequeueReusableCellWithIdentifier:noLeadIdentifier];
        if (!cell) {
            cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noLeadIdentifier];
        }
        cell.userNameLabel.text = [NSString stringWithFormat:@"No Accounts Found!"];//@"No Tasks Found"];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        cell.textLabel.textColor = [UIColor orangeColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    } else {
        CustomCell *cell=(CustomCell *)[tableView dequeueReusableCellWithIdentifier:leadIdentifier];
		if (cell == nil) {
            cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:leadIdentifier];
        }
        MMSF_Object *accountObj = (MMSF_Object *)[[fetchedResultsController fetchedObjects] objectAtIndex:indexPath.row];
        cell.userNameLabel.text = [accountObj valueForKey:@"Name"];
        if(indexPath.row%2){
            cell.userNameLabel.textColor = [UIColor whiteColor];
        }
        else{
            cell.userNameLabel.textColor = [UIColor greenColor];
        }
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
}

- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView {
    return 1;
}

- (NSInteger) tableView: (UITableView *) tableView numberOfRowsInSection: (NSInteger) section {
    if ([[fetchedResultsController fetchedObjects] count] > 0)
        return [[fetchedResultsController fetchedObjects] count];
    else
        return 1;
}

@end
