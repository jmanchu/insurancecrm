//
//  SFDCLeadsViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCLeadsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate> {
    IBOutlet UITableView *aTableView;
    IBOutlet UIButton *addButton;
}
@property (strong, nonatomic) IBOutlet UIButton *addButton;
- (IBAction)refreshButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)createLead:(id)sender;
/*
 * Call after each refresh sync
 */
-(void)reloadLeadViewWithNewData;
@end
