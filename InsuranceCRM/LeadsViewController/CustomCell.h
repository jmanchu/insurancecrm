//
//  CustomCell.h
//  InsuranceCRM
//
//  Created by Amisha Goyal on 2/5/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
{
UILabel *userNameLabel;
UIImageView *bgImageView;
}
@property (nonatomic, retain) UILabel *userNameLabel;
@property (nonatomic, retain) UIImageView *bgImageView;

@end
