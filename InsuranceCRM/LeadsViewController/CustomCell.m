//
//  CustomCell.m
//  InsuranceCRM
//
//  Created by Amisha Goyal on 2/5/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

@synthesize userNameLabel,bgImageView;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIView *myContentView = self.contentView;
        
        CGFloat bgWidth;
        CGFloat userNameWidth;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            bgWidth = 530;
            userNameWidth = 380;
        } else {
            bgWidth = 300;
            userNameWidth = 200;
        }
        self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 5.0, bgWidth, 40.0)];
        self.bgImageView.image = [UIImage imageNamed:@"Lead_List_Text_Base.png"];
        [myContentView addSubview:self.bgImageView];
        
        self.userNameLabel = [[UILabel alloc] init];
        [self.userNameLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
		// default
        [self.userNameLabel setFrame:CGRectMake(40.0, 7.0, userNameWidth, 30.0)];
        self.userNameLabel.backgroundColor=[UIColor clearColor];
        self.userNameLabel.textColor = [UIColor whiteColor];
		[myContentView addSubview:self.userNameLabel];
        
    }
    return self;
}

-(void)assignLayoutWithFrame:(CGRect)frame{
    //NSLog(@"In Assign \n\n\nself bounds  :%f  %f  self frame  :%f  %f",self.bounds.size.width,self.bounds.origin.x,self.frame.size.width,self.frame.origin.x);
    
    [self.userNameLabel setFrame:CGRectMake(40.0, 7.0, 380.0, 30.0)];
    // [self.dotImageView setFrame:CGRectMake(260.0, 15.0, 17.0, 17.0)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end