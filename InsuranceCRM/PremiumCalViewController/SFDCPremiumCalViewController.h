//
//  SFDCPremiumCalViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCPremiumCalViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    IBOutlet UITextField *sumAssuredField;
    IBOutlet UIButton *policyTermInMonthsBtn;
    IBOutlet UIButton *poicyTypeBtn;
    IBOutlet UIButton *dateOfBirthBtn;
    
    IBOutlet UIPickerView *productListPicker;
    IBOutlet UIPickerView *policyTermPicker;
    IBOutlet UIDatePicker *dateOfBirthPicker;
    
    IBOutlet UILabel *myPickerViewTitleLabel;
    IBOutlet UILabel *premiumPayingLbl;
    IBOutlet UIView *myPickerView;
}
@property (strong, nonatomic) IBOutlet UIView *resultView;

- (IBAction)backButtonAction:(id)sender;
- (IBAction)synchAction:(id)sender;
- (IBAction)calculatePremium:(id)sender;
- (IBAction)pickerCancelAction:(id)sender;
- (IBAction)pickerDoneAction:(id)sender;

- (IBAction)policyListBtnAction:(id)sender;
- (IBAction)dateOfBirthBtnSelected:(id)sender;
- (IBAction)policyTermInMOnthsBtnAction:(id)sender;
- (IBAction)resetValues:(id)sender;

+ (SFDCPremiumCalViewController *)createPremiumCalViewControllerFrom:(id)parentViewController;

@end
