//
//  SFDCPremiumCalViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCPremiumCalViewController.h"
#import "SFDCAppDelegate.h"

static MMSF_Object *selectedAccount;
static NSString *selectedTerm;

@interface SFDCPremiumCalViewController ()
@property (nonatomic, strong) NSArray *policyListArray;
@property (nonatomic, strong) NSArray *durationArray;
@property (nonatomic, strong) UIView *intermediateView;
@property (nonatomic, strong) UIView *currentPicker;
@property (nonatomic, strong) UIPopoverController *popoverController;
@end

@implementation SFDCPremiumCalViewController
@synthesize policyListArray, intermediateView, currentPicker, durationArray, popoverController, resultView;

+ (SFDCPremiumCalViewController *)createPremiumCalViewControllerFrom:(id)parentViewController {
    SFDCPremiumCalViewController *createPremiumViewController = [[SFDCPremiumCalViewController alloc] initWithNibName:@"SFDCPremiumCalViewController" bundle:nil];
    return createPremiumViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Create the sort descriptors array.
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
    NSSortDescriptor *nameDesciptor = [NSSortDescriptor descriptorWithKey:@"Name" ascending: YES selector:@selector(caseInsensitiveCompare:)];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nameDesciptor, nil];
    
    self.policyListArray = [[[[MM_ContextManager sharedManager] mainContentContext] allObjectsOfType: @"Product__c" matchingPredicate:nil sortedBy:sortDescriptors] mutableCopy];
    
    self.durationArray = [[NSArray alloc] initWithObjects:@"1 Month", @"3 Months", @"6 Months", @"1 Year", nil];
    
    /*
     * Add MyPickerView
     */
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self.view addSubview:myPickerView];
        CGRect myPickerViewFrame = [myPickerView frame];
        myPickerViewFrame.origin.x = 0;
        myPickerViewFrame.origin.y = 460;
        [myPickerView setFrame:myPickerViewFrame];
        
        self.intermediateView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [self.intermediateView setBackgroundColor:[UIColor blackColor]];
        [self.view insertSubview:self.intermediateView belowSubview:myPickerView];
        [self.intermediateView setAlpha:0.0];
        [dateOfBirthPicker setDate:[NSDate date]];
    }
    [self hideAllPickers];
    [premiumPayingLbl setText:nil];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [resultView setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Utilities
- (void)showPopoverFromView:(UIButton *)view {
    if (!self.popoverController) {
        UIViewController* popoverContent = [[UIViewController alloc]init];
        popoverContent.view = myPickerView;
        popoverContent.contentSizeForViewInPopover = CGSizeMake(320, 266);
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:popoverContent];
    }
    [self.popoverController  presentPopoverFromRect:CGRectMake(view.frame.size.width/2, view.frame.size.height/2, 10, 10)
                                             inView:view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


- (void)hideAllPickers {
    [productListPicker setHidden:YES];
    [policyTermPicker setHidden:YES];
    [dateOfBirthPicker setHidden:YES];
}

- (void)hideMyPickerView {
    CGRect myPickerFrame = myPickerView.frame;
    myPickerFrame.origin.y = 460;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [myPickerView setFrame:myPickerFrame];
    } completion:^(BOOL finished) {
        [self.intermediateView setAlpha:0.0];
    }];
}

- (void)presentMyPickerView {
    CGRect myPickerFrame = myPickerView.frame;
    myPickerFrame.origin.y = 460 - myPickerFrame.size.height;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [myPickerView setFrame:myPickerFrame];
    } completion:^(BOOL finished) {
        [self.intermediateView setAlpha:0.2];
    }];
}

- (void)showPicker:(UIView *)picker
{
	// hide the current picker and show the new one
	if (currentPicker)
	{
		currentPicker.hidden = YES;
	}
	picker.hidden = NO;
	
	currentPicker = picker;	// remember the current picker so we can remove it later when another one is chosen
}


#pragma mark - Button Actions
- (IBAction)policyListBtnAction:(id)sender {
    [myPickerViewTitleLabel setText:@"Select Policy"];
    [self showPicker:productListPicker];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self showPopoverFromView:sender];
    } else {
        [self presentMyPickerView];
    }
}

- (IBAction)dateOfBirthBtnSelected:(id)sender {
    [myPickerViewTitleLabel setText:@"Select DOB"];
    [self showPicker:dateOfBirthPicker];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self showPopoverFromView:sender];
    } else {
        [self presentMyPickerView];
    }
}

- (IBAction)policyTermInMOnthsBtnAction:(id)sender {
    [myPickerViewTitleLabel setText:@"Select Term"];
    [self showPicker:policyTermPicker];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self showPopoverFromView:sender];
    } else {
        [self presentMyPickerView];
    }
}

- (IBAction)backButtonAction:(id)sender {
    [UIView animateWithDuration:0.75 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view setAlpha:0.0];
    } completion:^(BOOL finished) {
        [self.navigationController popViewControllerAnimated:NO];
    }];
}

- (IBAction)synchAction:(id)sender {
    SFDCAppDelegate *appDel = (SFDCAppDelegate *)([[UIApplication sharedApplication]delegate]);
    [appDel refreshAction];   
}

- (IBAction)calculatePremium:(id)sender {
    /*
     * Validate fields.
     */
    if ([[poicyTypeBtn titleForState:UIControlStateNormal] isEqualToString:@"Policy Type"]) {
        [SA_AlertView showAlertWithTitle:@"Select Policy Type" message:@"Policy Type shouldn't be empty"];
        return;
    }
    
    if ([[sumAssuredField text] isEqualToString:@""] || ![sumAssuredField text]) {
        [SA_AlertView showAlertWithTitle:@"Select Sum assured" message:@"Sum assured shouldn't be empty"];
        return;
    }
    
    if ([[dateOfBirthBtn titleForState:UIControlStateNormal] isEqualToString:@"Date of Birth"]) {
        [SA_AlertView showAlertWithTitle:@"Select Date of Birth" message:@"Date of Birth shouldn't be empty"];
        return;
    }
    
    if ([[policyTermInMonthsBtn titleForState:UIControlStateNormal] isEqualToString:@"Duration"]) {
        [SA_AlertView showAlertWithTitle:@"Select Policy Duration" message:@"Policy Duration shouldn't be empty"];
        return;
    }
    
    [premiumPayingLbl setText:@"8200"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [resultView setHidden:NO];
    }
}

- (IBAction)resetValues:(id)sender {
    selectedAccount = nil;
    selectedTerm = nil;
    [poicyTypeBtn setTitle:@"Policy Type" forState:UIControlStateNormal];
    [sumAssuredField setText:nil];
    [policyTermInMonthsBtn setTitle:@"Duration" forState:UIControlStateNormal];
    [dateOfBirthBtn setTitle:@"Date of Birth" forState:UIControlStateNormal];
    [premiumPayingLbl setText:nil];
    
    [resultView setHidden:YES];
}

- (IBAction)pickerCancelAction:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    } else {
        [self hideMyPickerView];
    }
}

- (IBAction)pickerDoneAction:(id)sender {
    if (currentPicker == productListPicker) {
        [poicyTypeBtn setTitle:[selectedAccount valueForKey:@"Name"] forState:UIControlStateNormal];
    }
    if (currentPicker == policyTermPicker) {
        [policyTermInMonthsBtn setTitle:selectedTerm forState:UIControlStateNormal];
    }
    if (currentPicker == dateOfBirthPicker) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, YYYY"];
        NSString *dateString = [dateFormat stringFromDate:[dateOfBirthPicker date]];
        [dateOfBirthBtn setTitle:dateString forState:UIControlStateNormal];
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    } else {
        [self hideMyPickerView];
    }
}

#pragma mark - UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual:productListPicker]) {
        selectedAccount = (MMSF_Object *)[policyListArray objectAtIndex:row];
    } else {
        selectedTerm = [durationArray objectAtIndex:row];
    }
}


#pragma mark - UIPickerViewDataSource

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *returnStr = @"";
	if ([pickerView isEqual:productListPicker]) {
        returnStr = [(MMSF_Object *)[policyListArray objectAtIndex:row] valueForKey:@"Name"];
    } else {
        returnStr = [durationArray objectAtIndex:row];
    }
	return returnStr;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat componentWidth = 0.0;
    
    if ([pickerView isEqual:productListPicker]) {
        componentWidth = 320;
    } else {
        componentWidth = 320;
    }
    
	return componentWidth;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger count;
    if ([pickerView isEqual:productListPicker]) {
        count = [policyListArray count];
    } else {
        count = [durationArray count];
    }
    return count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}


#pragma mark - UITextFieldDelegate
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField isEqual:sumAssuredField]) {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if(theTextField == sumAssuredField) {
        [sumAssuredField resignFirstResponder];
    }
    return YES;
}


- (void)viewDidUnload {
    [self setResultView:nil];
    [super viewDidUnload];
}
@end
