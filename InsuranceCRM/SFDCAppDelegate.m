//
//  SFDCAppDelegate.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 12/24/12.
//  Copyright (c) 2012 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCAppDelegate.h"
#import "SFDCLoginViewController.h"
#import "SFDCHomeViewController.h"
#import "SFDCHomeViewController_iPad.h"
#import "RKReachabilityObserver.h"
#import "Reachability.h"
#import "RKRequestQueue.h"

@implementation SFDCAppDelegate
@synthesize navigationController, loginViewController, homeViewController;

#pragma mark - Sync/Login
- (void)setLoginParameters {
    [MM_LoginViewController setRedirectURI:kOAuthRedirectURI];
    [MM_LoginViewController setRemoteAccessConsumerKey:kRemoteAccessConsumerKey];
    [MM_LoginViewController setLoginDomain:kOAuthLoginDomain];
}

- (void) performSync: (BOOL) full
{
    
    if (full) {
        [[MM_SyncManager sharedManager] fullResync: nil withCommpletionBlock: nil];
        return;
    }
    
    simpleBlock fetchAndSyncBlock = ^{
        [[MM_SyncManager sharedManager] fetchRequiredMetaData: NO withCompletionBlock: ^{
            [[MM_SyncManager sharedManager] synchronize: nil withCompletionBlock: nil];
        }];
    };
    
    [SA_ConnectionQueue sharedQueue].activityIndicatorCount++;
    if (![MM_OrgMetaData sharedMetaData].areAllObjectsToSyncPresent) {
        [[MM_SyncManager sharedManager] downloadObjectDefinitionsWithCompletionBlock: fetchAndSyncBlock];
    } else {
        fetchAndSyncBlock();
    }
}

- (void) connectWithSalesForce
{
    NSString  *lastUserSync = [[NSUserDefaults standardUserDefaults] stringForKey: kDefaults_PreviousUserName];
    if (![lastUserSync isEqualToString: [SFOAuthCoordinator fullUserId]])
    {
        if(lastUserSync!= nil)
        {
            LOG(@"User has changed since last sync.");
            [self reSyncWithSalesForce];
            return;
        }
    }
    else if(![MM_Config sharedManager].startupSyncRequired){
        LOG(@"Last User logged in again");
        [self performSync:NO];
        return;
    }
    [self syncWithSalesForce];
}


- (void) reSyncWithSalesForce
{
    // When the user changes, we need to delete the data
    booleanArgumentBlock downloadBlock = ^void (BOOL value){
        if(value)
        {
            //Need to move this
            [[NSUserDefaults standardUserDefaults] setValue: [SFOAuthCoordinator fullUserId] forKey: kDefaults_PreviousUserName];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    };
    
    // Fetch the meta Data and Sync with the help of queries in the Plist (sync_objects)
    simpleBlock			fetchAndSyncBlock = ^{
        [[MM_SyncManager sharedManager] fetchRequiredMetaData: YES withCompletionBlock: ^{
            [[MM_SyncManager sharedManager] resyncAllDataWithCompletionBlock:downloadBlock];
            NSLog(@"sync");
            
            [SA_ConnectionQueue sharedQueue].activityIndicatorCount--;
        }];
    };
    
    //Initiate the global Objects Download
	[SA_ConnectionQueue sharedQueue].activityIndicatorCount++;
	if (![MM_OrgMetaData sharedMetaData].areAllObjectsToSyncPresent) {
		[[MM_SyncManager sharedManager] downloadObjectDefinitionsWithCompletionBlock: fetchAndSyncBlock];
	} else {
		fetchAndSyncBlock();
	}
}




- (void) syncWithSalesForce
{
    // When the user changes, we need to delete the data
    booleanArgumentBlock downloadBlock = ^void (BOOL value){
        if(value)
        {
            //Need to move this
            [[NSUserDefaults standardUserDefaults] setValue: [SFOAuthCoordinator fullUserId] forKey: kDefaults_PreviousUserName];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    };
    // Fetch the meta Data and Sync with the help of queries in the Plist (sync_objects)
    simpleBlock			fetchAndSyncBlock = ^{
        [[MM_SyncManager sharedManager] fetchRequiredMetaData: NO withCompletionBlock: ^{
            [[MM_SyncManager sharedManager] synchronize: nil withCompletionBlock: downloadBlock];
            NSLog(@"sync");
            
            [SA_ConnectionQueue sharedQueue].activityIndicatorCount--;
        }];
    };
    
    //Initiate the global Objects Download
	[SA_ConnectionQueue sharedQueue].activityIndicatorCount++;
	if (![MM_OrgMetaData sharedMetaData].areAllObjectsToSyncPresent) {
		[[MM_SyncManager sharedManager] downloadObjectDefinitionsWithCompletionBlock: fetchAndSyncBlock];
	} else {
		fetchAndSyncBlock();
	}
}

- (void)loginCompleted {
    /*
     * Initiate syncing.
     */
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    MM_LoginViewController   *controller = [MM_LoginViewController currentController];
    if(controller.useTestServer){
        [userDef setBool:YES forKey:@"Sandbox"];
        [userDef synchronize];
    }
    else{
        [userDef setBool:NO forKey:@"Sandbox"];
        [userDef synchronize];
    }
    [userDef setBool:YES forKey:@"LoginStatus"];
    [userDef synchronize];
//    /*
//     * When login finishes, load Home view.i.e, SFDCHomeViewController.
//     */
//    if (loginViewController) {
//        self.homeViewController = [[SFDCHomeViewController alloc] initWithNibName:@"SFDCHomeViewController" bundle:nil];
//        self.navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
//        [self.navigationController setNavigationBarHidden:YES];
//        [self.window setRootViewController:navigationController];
//        [[NSNotificationCenter defaultCenter] removeObserver:loginViewController name:UIApplicationWillEnterForegroundNotification object:nil];
//        self.loginViewController = nil;
//    }
    [self connectWithSalesForce];
}


- (void) handlePleaseWaitDisplayWithText:(NSString*)text andProgressBarValue:(float)value
{
    [[SA_PleaseWaitDisplay pleaseWaitDisplay] setProgressValueAsNumber: [NSNumber numberWithFloat: value]];
    if(value > 0.0)
    {
        [SA_PleaseWaitDisplay showPleaseWaitDisplayWithMajorText: @"Loading From Salesforce..." minorText:text cancelLabel: nil showProgressBar: YES delegate: nil];
    }
    else {
        [SA_PleaseWaitDisplay showPleaseWaitDisplayWithMajorText: @"Loading From Salesforce..." minorText:text cancelLabel: nil showProgressBar: NO delegate: nil];
    }
}

- (void) hideThePleaseWaitDisplay
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SA_PleaseWaitDisplay hidePleaseWaitDisplay];
    });
}

- (void)syncBegan:(NSNotification*)notification {
    /*
     * Show activity indictor to indicate pregress in download.
     */
    dispatch_async(dispatch_get_main_queue(), ^{
        [self handlePleaseWaitDisplayWithText:@"Sync Started" andProgressBarValue:0.0];
    });
}

- (void)syncEnded:(NSNotification*)notification {
    /*
     * Hide activity indictor.
     */
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self syncEnded:notification];
        });
		return;
	}
    
    [self hideThePleaseWaitDisplay];
    
    /*
     * When login finishes, load Home view.i.e, SFDCHomeViewController.
     */
    if (!self.homeViewController) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            self.homeViewController = [[SFDCHomeViewController_iPad alloc] initWithNibName:@"SFDCHomeViewController_iPad" bundle:nil];
        } else {
            self.homeViewController = [[SFDCHomeViewController alloc] initWithNibName:@"SFDCHomeViewController" bundle:nil];
        }
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
        [self.navigationController setNavigationBarHidden:YES];
        [self.window setRootViewController:navigationController];
        [[NSNotificationCenter defaultCenter] removeObserver:loginViewController name:UIApplicationWillEnterForegroundNotification object:nil];
        self.loginViewController = nil;
    }
}

-(void)syncPaused {
    BOOL online = ![SA_ConnectionQueue sharedQueue].offline;
	if (!online) {
        [SA_PleaseWaitDisplay hidePleaseWaitDisplay];
    }
}

- (void)willRemoveAllData:(id)sender {
    
}

-(void)passOnToLogin:(id)sender {
    
    NetworkStatus internetConnectionStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    if (internetConnectionStatus == NotReachable)
    {
        UIAlertView *error_alert = [[UIAlertView alloc] initWithTitle:nil message:@"Internet connection not availble.\n\n"
                                                             delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [error_alert show];
        [error_alert release];
        
    }
    else {
        [MM_LoginViewController logout];
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        [userDef setBool:NO forKey:@"LoginStatus"];
        [userDef synchronize];
    }
}

- (void) networkStatusChanged:(NSNotification *) note {
    if ([[MM_SyncManager sharedManager] isSyncInProgress]) {
        if([UIDevice connectionType] == connection_none) {
            LOG(@"Reachability Changed: Sync interrupted: connection type: %d", [UIDevice connectionType]);
            //Cancel pending transactions, waiting on Update for REST framework
            [[SA_ConnectionQueue sharedQueue] cancelAllConnections];
            [[RKRequestQueue requestQueue] cancelAllRequests];
            [SA_AlertView showAlertWithTitle: @""
                                     message: @"Network connection disrupted during a synchronization operation. Please sync again once connected."];
            [SA_PleaseWaitDisplay hidePleaseWaitDisplay];
        } else {
            NSLog(@"Reachability Changed: connection type: %d", [UIDevice connectionType]);
        }
    }
}

- (void)loadLoginView {
    [MM_LoginViewController setRedirectURI:kOAuthRedirectURI];
    [MM_LoginViewController setRemoteAccessConsumerKey:kRemoteAccessConsumerKey];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    
    if ([userDef boolForKey:@"LoginStatus"]) {
        if (![userDef boolForKey:@"Sandbox"]) {
            [MM_LoginViewController setLoginDomain:kOAuthLoginDomain];
        }else{
            [MM_LoginViewController setLoginDomain:@"test.salesforce.com"];
        }
    }
    else{
        [MM_LoginViewController setLoginDomain:kOAuthLoginDomain];
    }
    
    MM_LoginViewController   *controller = [MM_LoginViewController presentModallyInParent:self.navigationController];
    
    if ([userDef boolForKey:@"LoginStatus"]) {
        if (![userDef boolForKey:@"Sandbox"]) {
            controller.useTestServer = NO;
        }else{
            controller.useTestServer = YES;
        }
    }
    else{
        controller.useTestServer = NO;
    }
    
#if DEBUG
    
    controller.forceLoginOnFreshInstall = YES;
    controller.canToggleServer = YES;
#else
    controller.forceLoginOnFreshInstall = YES;
    controller.canToggleServer = YES;
#endif
    controller.canCancel = NO;
    controller.navigationBar.topItem.leftBarButtonItem = nil;
    [[controller serverToggleSwitch] setOn:NO];
    
    
    [userDef setBool:NO forKey:@"LoginStatus"];
    [userDef synchronize];
}

-(void)refreshAction {
    NetworkStatus internetConnectionStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    if (internetConnectionStatus == NotReachable)
    {
        UIAlertView *error_alert = [[UIAlertView alloc] initWithTitle:nil message:@"Internet connection not availble.\n\n"
                                                             delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [error_alert show];
        [error_alert release];
        
    }
    else {
        [self handlePleaseWaitDisplayWithText:@"Sync Started" andProgressBarValue:0.0];
        [self cancelAndPerformSelector:@selector(loadLoginView) withObject:nil afterDelay:0.5];
        
    }
    
}


#pragma mark - App Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [self setLoginParameters];
    if (![userDef boolForKey:@"LoginStatus"]) {
        NSString *loginViewNibName = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            loginViewNibName = @"SFDCLoginViewController-iPad";
        } else {
            loginViewNibName = @"SFDCLoginViewController";
        }
        self.loginViewController = [[SFDCLoginViewController alloc] initWithNibName:loginViewNibName bundle:nil];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    } else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            self.homeViewController = [[SFDCHomeViewController_iPad alloc] initWithNibName:@"SFDCHomeViewController_iPad" bundle:nil];
        } else {
            self.homeViewController = [[SFDCHomeViewController alloc] initWithNibName:@"SFDCHomeViewController" bundle:nil];
        }
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    }
    [self.navigationController setNavigationBarHidden:YES];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [imageView setImage:[UIImage imageNamed:@"BG.png"]];
    [self.window addSubview:imageView];
    
    // Override point for customization after application launch.
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    
    /*
     * Subscribe to Notification Center for
     * - Login_Complete
     * - Sync_Began
     * - Sync_Complete
     * - WillRemoveAllData
     * - OrgSyncObjectsChanged
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginCompleted) name:kNotification_DidAuthenticate object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncBegan:) name:kNotification_SyncBegan object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncBegan:) name:kNotification_OrgSyncObjectsChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncEnded:) name:kNotification_SyncComplete object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncCanceled:) name:kNotification_SyncCancelled object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncPaused:) name:kNotification_SyncPaused object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRemoveAllData:) name:kNotification_WillRemoveAllData object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(networkStatusChanged:) name: RKReachabilityDidChangeNotification object: nil];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
