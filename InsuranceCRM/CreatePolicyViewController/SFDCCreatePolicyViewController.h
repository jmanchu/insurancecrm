//
//  SFDCCreatePolicyViewController.h
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFDCCreatePolicyViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *doneButton;
    
    IBOutlet UIButton *pickAccountBtn;
    IBOutlet UIButton *pickPolicyBtn;
    
    IBOutlet UIScrollView *aScrollView;
    IBOutlet UITextField *sumAssuredField;
    IBOutlet UIButton *startDateButton;
    IBOutlet UIButton *policyTermButton;
    
    IBOutlet UIButton *imageCaptureBtn;
    IBOutlet UIButton *documentPickerBtn;
    IBOutlet UIButton *digitalSignBtn;
    IBOutlet UIButton *geoLocationBtn;
    
    IBOutlet UIView *myPickerView;
    IBOutlet UIButton *myPickerCancelBtn;
    IBOutlet UILabel *myPickerViewTitleLabel;
    IBOutlet UIButton *myPickerDoneBtn;
    IBOutlet UIPickerView *accountListPicker;
    IBOutlet UIPickerView *policyListPicker;
    IBOutlet UIPickerView *policyTermPicker;
    IBOutlet UIDatePicker *myDatePicker;
}

- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)doneButtonAction:(id)sender;

- (IBAction)pickAccountBtnAction:(id)sender;
- (IBAction)pickPolicyBtnAction:(id)sender;
- (IBAction)pickPolicyTermAction:(id)sender;
- (IBAction)startDateBtnAction:(id)sender;

- (IBAction)imageCaptureBtnAction:(id)sender;
- (IBAction)documentPickerBtnAction:(id)sender;
- (IBAction)digitalSignBtnAction:(id)sender;
- (IBAction)geoLocationBtnAction:(id)sender;

- (IBAction)myPickerCancelBtnAction:(id)sender;
- (IBAction)myPickerDoneBtnAction:(id)sender;

@property (nonatomic, strong)IBOutlet UIButton *doneButton;
@property (nonatomic, strong)IBOutlet UILabel *titleLabel;

+ (SFDCCreatePolicyViewController *)viewPolicyViewControllerFrom:(id)parentViewController modallyUsingPolicy:(MMSF_Object *)policyObj;
+ (SFDCCreatePolicyViewController *)createPolicyViewControllerFrom:(id)parentViewController modallyUsingAccount:(MMSF_Object *)accountObj;
+ (SFDCCreatePolicyViewController *)createPolicyViewControllerFrom:(id)parentViewController;
+ (SFDCCreatePolicyViewController *)createPolicyViewControllerFrom:(id)parentViewController modallyUsingProduct:(MMSF_Object *)policyObj;

@end
