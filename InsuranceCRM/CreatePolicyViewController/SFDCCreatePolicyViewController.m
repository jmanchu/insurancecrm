//
//  SFDCCreatePolicyViewController.m
//  InsuranceCRM
//
//  Created by Jayaprakash Manchu on 1/3/13.
//  Copyright (c) 2013 Jayaprakash Manchu. All rights reserved.
//

#import "SFDCCreatePolicyViewController.h"
#import "SFDCDigitalSignatureViewController.h"
#import "SFDCCaptureImagesViewController.h"

static MMSF_Object *selectedAccount;
static MMSF_Object *selectedPolicy;
static NSString *policyTermString;
MMSF_Object *newPolicy;
MMSF_Object *displayPolicyObj;
NSManagedObjectContext *context;

BOOL isModelView;
BOOL isNewPolicy;

@interface SFDCCreatePolicyViewController ()
@property (nonatomic, strong) NSArray *accountListArray;
@property (nonatomic, strong) NSArray *policyListArray;
@property (nonatomic, strong) NSArray *policyTermArray;
@property (nonatomic, strong) UIView *intermediateView;
@property (nonatomic, strong) UIView *currentPicker;
@property (nonatomic, strong) UIPopoverController *popoverController;
@end

@implementation SFDCCreatePolicyViewController
@synthesize accountListArray, policyListArray, policyTermArray, intermediateView, currentPicker, popoverController, doneButton, titleLabel;

#pragma mark - Initialization
+ (SFDCCreatePolicyViewController *)viewPolicyViewControllerFrom:(id)parentViewController modallyUsingPolicy:(MMSF_Object *)policyObj {
    SFDCCreatePolicyViewController *createPolicyViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController_iPadFS" bundle:nil];
    } else {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController" bundle:nil];
    }
    isModelView = NO;
    displayPolicyObj = policyObj;
    selectedAccount = nil;
    selectedPolicy = nil;
    isNewPolicy = NO;
    return createPolicyViewController;
}

+ (SFDCCreatePolicyViewController *)createPolicyViewControllerFrom:(id)parentViewController modallyUsingAccount:(MMSF_Object *)accountObj {
    SFDCCreatePolicyViewController *createPolicyViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController_iPadFS" bundle:nil];
    } else {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController" bundle:nil];
    }
    isModelView = YES;
    displayPolicyObj = nil;
    selectedAccount = accountObj;
    selectedPolicy = nil;
    isNewPolicy = YES;
    return createPolicyViewController;
}

+ (SFDCCreatePolicyViewController *)createPolicyViewControllerFrom:(id)parentViewController {
    SFDCCreatePolicyViewController *createPolicyViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController_iPad" bundle:nil];
    } else {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController" bundle:nil];
    }
    
    isModelView = NO;
    displayPolicyObj = nil;
    selectedAccount = nil;
    selectedPolicy = nil;
    isNewPolicy = YES;
    return createPolicyViewController;
}

+ (SFDCCreatePolicyViewController *)createPolicyViewControllerFrom:(id)parentViewController modallyUsingProduct:(MMSF_Object *)policyObj {
    SFDCCreatePolicyViewController *createPolicyViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController_iPadFS" bundle:nil];
    } else {
        createPolicyViewController = [[SFDCCreatePolicyViewController alloc] initWithNibName:@"SFDCCreatePolicyViewController" bundle:nil];
    }
    isModelView = YES;
    displayPolicyObj = nil;
    selectedAccount = nil;
    selectedPolicy = policyObj;
    isNewPolicy = YES;
    return createPolicyViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
     * Fetch all Accounts
     */
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(canProceedWithSaving:) name: kNotification_ObjectCreated object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(objectSavingFailed:) name: kNotification_ObjectSaveError object: nil];

    
    NSPredicate	*predicate = nil;
    NSSortDescriptor *dateDescriptor = nil;
    NSSortDescriptor *nameDesciptor = nil;
    NSArray *sortDescriptors = nil;
    
    if (displayPolicyObj) {
        [self.doneButton setHidden:YES];
        selectedAccount = [displayPolicyObj valueForKey:@"AccountId"];
        [self.titleLabel setText:[selectedAccount valueForKey:@"Name"]];
        
        selectedPolicy = [displayPolicyObj valueForKey:@"Product__c"];
        [sumAssuredField setText:[(NSNumber *)[displayPolicyObj valueForKey:@"Sum_Assured__c"] stringValue]];
        [sumAssuredField setBorderStyle:UITextBorderStyleNone];
        [sumAssuredField setUserInteractionEnabled:NO];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, YYYY"];
        NSString *dateString = [dateFormat stringFromDate:[displayPolicyObj valueForKey:@"StartDate"]];
        [startDateButton setTitle:dateString forState:UIControlStateNormal];
        [startDateButton setUserInteractionEnabled:NO];
        
        [policyTermButton setTitle:[(NSNumber *)[displayPolicyObj valueForKey:@"ContractTerm"] stringValue] forState:UIControlStateNormal];
        [policyTermButton setUserInteractionEnabled:NO];
    }
    
    if (selectedAccount) {
        context = [selectedAccount moc];
    } else if (selectedPolicy) {
        context = [selectedPolicy moc];
    } else {
        context = [[MM_ContextManager sharedManager] contentContextForWriting];
    }
    /*
     * Fetch all Accounts.
     */
    if (!selectedAccount) {
        MMSF_Object *currentUser = [MM_SyncManager currentUserInContext:context];
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:$A($P(@"OwnerId == %@",currentUser), nil)];
        // Create the sort descriptors array.
        dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
        nameDesciptor = [NSSortDescriptor descriptorWithKey:@"Name" ascending: YES selector:@selector(caseInsensitiveCompare:)];
        sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nameDesciptor, nil];
        
        self.accountListArray = [[context allObjectsOfType: @"Account" matchingPredicate: predicate sortedBy:sortDescriptors] mutableCopy];
    } else {
        [pickAccountBtn setBackgroundImage:[UIImage imageNamed:@"Policies_Btn.png"] forState:UIControlStateNormal];
        [pickAccountBtn setTitle:[selectedAccount valueForKey:@"Name"] forState:UIControlStateNormal];
        [pickAccountBtn setUserInteractionEnabled:NO];
    }
    
    /*
     * Fetch all Policies.
     */
    if (!selectedPolicy) {
        dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastModifiedDate" ascending:YES];
        nameDesciptor = [NSSortDescriptor descriptorWithKey:@"Name" ascending: YES selector:@selector(caseInsensitiveCompare:)];
        sortDescriptors = [[NSArray alloc] initWithObjects:dateDescriptor, nameDesciptor, nil];
        self.policyListArray = [[context allObjectsOfType: @"Product__c" matchingPredicate:nil sortedBy:sortDescriptors] mutableCopy];
    } else {
        [pickPolicyBtn setBackgroundImage:[UIImage imageNamed:@"Policies_Btn.png"] forState:UIControlStateNormal];
        [pickPolicyBtn setTitle:[selectedPolicy valueForKey:@"Name"] forState:UIControlStateNormal];
        [pickPolicyBtn setUserInteractionEnabled:NO];
    }
    
    self.policyTermArray = [[NSArray alloc] initWithObjects:@"1",@"3",@"6",@"12",@"24",@"36",@"48",@"60",@"72",@"84",@"96",@"108",@"120",@"132",@"144",@"156",@"168",@"180",@"192",@"204",@"216",@"228",@"240", nil];
    
    
    if (isNewPolicy) {
        [imageCaptureBtn setEnabled:NO];
        [documentPickerBtn setEnabled:NO];
        [digitalSignBtn setEnabled:NO];
        [geoLocationBtn setEnabled:NO];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            /*
             * Add MyPickerView
             */
            [self.view addSubview:myPickerView];
            CGRect myPickerViewFrame = [myPickerView frame];
            myPickerViewFrame.origin.x = 0;
            myPickerViewFrame.origin.y = 460;
            [myPickerView setFrame:myPickerViewFrame];
            
            self.intermediateView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            [self.intermediateView setBackgroundColor:[UIColor blackColor]];
            [self.view insertSubview:self.intermediateView belowSubview:myPickerView];
            [self.intermediateView setAlpha:0.0];
        }
        [myDatePicker setDate:[NSDate date]];
        [self hideAllPickers];
    } else {
        [imageCaptureBtn setEnabled:YES];
        [documentPickerBtn setEnabled:YES];
        [digitalSignBtn setEnabled:YES];
        [geoLocationBtn setEnabled:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notifications
- (void) canProceedWithSaving: (NSNotification *) note {
    /*
     * Policy created successfully, enable buttons to add attachments & disable user interaction on all other fileds.
     */
    [imageCaptureBtn setEnabled:YES];
    [documentPickerBtn setEnabled:YES];
    [digitalSignBtn setEnabled:YES];
    [geoLocationBtn setEnabled:YES];
    
    [pickAccountBtn setBackgroundImage:[UIImage imageNamed:@"Policies_Btn.png"] forState:UIControlStateNormal];
    [pickAccountBtn setUserInteractionEnabled:NO];
    [pickPolicyBtn setBackgroundImage:[UIImage imageNamed:@"Policies_Btn.png"] forState:UIControlStateNormal];
    [pickPolicyBtn setUserInteractionEnabled:NO];
    [startDateButton setUserInteractionEnabled:NO];
    [policyTermButton setUserInteractionEnabled:NO];
    [sumAssuredField setUserInteractionEnabled:NO];
    [sumAssuredField setBorderStyle:UITextBorderStyleNone];
    [doneButton setEnabled:NO];
    [SA_AlertView showAlertWithTitle:@"Policy Created Successfully!" message:nil];
}

- (void) objectSavingFailed: (NSNotification *) note {
    if (newPolicy != nil) {
        [newPolicy deleteFromContext];
        newPolicy = nil;
    }
}


#pragma mark - Utilities
- (void)showPopoverFromView:(UIButton *)view {
    if (!self.popoverController) {
        UIViewController* popoverContent = [[UIViewController alloc]init];
        popoverContent.view = myPickerView;
        popoverContent.contentSizeForViewInPopover = CGSizeMake(320, 266);
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:popoverContent];
    }
    [self.popoverController  presentPopoverFromRect:CGRectMake(view.frame.size.width/2, view.frame.size.height/2, 10, 10)
                                             inView:view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


- (void)hideAllPickers {
    [accountListPicker setHidden:YES];
    [policyListPicker setHidden:YES];
    [policyTermPicker setHidden:YES];
    [myDatePicker setHidden:YES];
}

- (void)hideMyPickerView {
    [aScrollView setContentOffset:CGPointMake(0.0, 0.0)];
    CGRect myPickerFrame = myPickerView.frame;
    myPickerFrame.origin.y = 460;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [myPickerView setFrame:myPickerFrame];
    } completion:^(BOOL finished) {
        [self.intermediateView setAlpha:0.0];
    }];
}

- (void)presentMyPickerView {
    CGRect myPickerFrame = myPickerView.frame;
    myPickerFrame.origin.y = 460 - myPickerFrame.size.height;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [myPickerView setFrame:myPickerFrame];
    } completion:^(BOOL finished) {
        [self.intermediateView setAlpha:0.2];
    }];
}

- (void)showPicker:(UIView *)picker
{
	// hide the current picker and show the new one
    [self hideAllPickers];
	if (currentPicker)
	{
		currentPicker.hidden = YES;
	}
	picker.hidden = NO;
	
	currentPicker = picker;	// remember the current picker so we can remove it later when another one is chosen
}

#pragma mark - Validations
- (BOOL) validateDataFields {
    
    if (!selectedAccount) {
        [SA_AlertView showAlertWithTitle:@"No Account Selected!" message:@"Account Name should not be empty!!"];
        [self pickAccountBtnAction:pickAccountBtn];
        return NO;
    }
    if (!selectedPolicy) {
        [SA_AlertView showAlertWithTitle:@"No Policy Selected!" message:@"Policy Name should not be empty!!"];
        [self pickPolicyBtnAction:pickPolicyBtn];
        return NO;
    }
    
    if (![[sumAssuredField text] length]) {
        [SA_AlertView showAlertWithTitle:@"No Assured Amount!" message:@"Assured Amount should not be empty!!"];
        [sumAssuredField becomeFirstResponder];
        return NO;
    }
    if ([[startDateButton titleForState:UIControlStateNormal] isEqualToString:@"Start Date"]) {
        [SA_AlertView showAlertWithTitle:@"No Start date!" message:@"Start date should not be empty!!"];
        [self startDateBtnAction:startDateButton];
        return NO;
    }
    
    if (![policyTermString length]) {
        [SA_AlertView showAlertWithTitle:@"No Policy Term!" message:@"Policy Term should not be empty!!"];
        [self pickPolicyTermAction:policyTermButton];
        return NO;
    }
    
    [sumAssuredField setBorderStyle:UITextBorderStyleNone];
    [sumAssuredField setUserInteractionEnabled:NO];
    
    newPolicy = (MMSF_Object*)[context insertNewEntityWithName:@"Contract"];
    [newPolicy beginEditing];
    
    [newPolicy setValue:selectedAccount forKey:@"AccountId"];
    [newPolicy setValue:selectedPolicy forKey:@"Product__c"];
    
    [newPolicy setValue:[NSNumber numberWithFloat:[sumAssuredField.text floatValue]] forKey:@"Sum_Assured__c"];
    [newPolicy setValue:[myDatePicker date] forKey:@"StartDate"];
    [newPolicy setValue:[NSNumber numberWithInt:[policyTermString intValue]] forKey:@"ContractTerm"];
    
    [newPolicy finishEditingSavingChanges:YES];
    
    return YES;
}

#pragma mark - Button Actions
- (void)dismissModalViewWithAnimation {
    if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self dismissModalViewControllerAnimated:YES];
    } else if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (IBAction)cancelButtonAction:(id)sender {
    if (isModelView) {
        [self dismissModalViewWithAnimation];
    } else {
        [UIView animateWithDuration:0.75 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.view setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.navigationController popViewControllerAnimated:NO];
        }];
    }
}

- (IBAction)doneButtonAction:(id)sender {
    /*
     * Validate data & save the new object.
     */
    [self validateDataFields];
}

- (IBAction)pickAccountBtnAction:(id)sender {
    [myPickerViewTitleLabel setText:@"Select Account"];
    [self showPicker:accountListPicker];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self showPopoverFromView:sender];
    } else {
        [self presentMyPickerView];
    }
}

- (IBAction)pickPolicyBtnAction:(id)sender {
    [myPickerViewTitleLabel setText:@"Select Policy Type"];
    [self showPicker:policyListPicker];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self showPopoverFromView:sender];
    } else {
        [self presentMyPickerView];
    }
}

- (IBAction)startDateBtnAction:(id)sender {
    [aScrollView setContentOffset:CGPointMake(0.0, 60.0)];
    [myPickerViewTitleLabel setText:@"Select Start Date"];
    [self showPicker:myDatePicker];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self showPopoverFromView:sender];
    } else {
        [self presentMyPickerView];
    }
}

- (IBAction)pickPolicyTermAction:(id)sender {
    [aScrollView setContentOffset:CGPointMake(0.0, 80.0)];
    [myPickerViewTitleLabel setText:@"Select Policy Term"];
    [self showPicker:policyTermPicker];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self showPopoverFromView:sender];
    } else {
        [self presentMyPickerView];
    }
}


#pragma mark - Attachments
- (IBAction)imageCaptureBtnAction:(id)sender {
    MMSF_Object *policy;
    if (!newPolicy) {
        policy = displayPolicyObj;
    } else {
        policy = newPolicy;
    }
    SFDCCaptureImagesViewController *captureImgViewController = [SFDCCaptureImagesViewController createControllerWithTitle:@"Photos" withContractObj:policy];
    [self.navigationController pushViewController:captureImgViewController animated:YES];
}

- (IBAction)documentPickerBtnAction:(id)sender {
    MMSF_Object *policy;
    if (!newPolicy) {
        policy = displayPolicyObj;
    } else {
        policy = newPolicy;
    }
    SFDCCaptureImagesViewController *captureImgViewController = [SFDCCaptureImagesViewController createControllerWithTitle:@"Attachments" withContractObj:policy];
    [self.navigationController pushViewController:captureImgViewController animated:YES];
}

- (IBAction)digitalSignBtnAction:(id)sender {
    MMSF_Object *policy;
    if (!newPolicy) {
        policy = displayPolicyObj;
    } else {
        policy = newPolicy;
    }
    SFDCDigitalSignatureViewController *digitalSignViewController = [SFDCDigitalSignatureViewController createDigitalSignatureControllerUsingParentId:policy];
    [self.navigationController pushViewController:digitalSignViewController animated:YES];
}

- (IBAction)geoLocationBtnAction:(id)sender {
    
}

#pragma mark - Picker Actions
- (IBAction)myPickerCancelBtnAction:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    } else {
        [self hideMyPickerView];
    }
}

- (IBAction)myPickerDoneBtnAction:(id)sender {
    if ((currentPicker == policyListPicker) && selectedPolicy) {
        [pickPolicyBtn setTitle:[selectedPolicy valueForKey:@"Name"] forState:UIControlStateNormal];
    }
    if ((currentPicker == accountListPicker) && selectedAccount) {
        [pickAccountBtn setTitle:[selectedAccount valueForKey:@"Name"] forState:UIControlStateNormal];
    }
    if (currentPicker == myDatePicker) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, YYYY"];
        NSString *dateString = [dateFormat stringFromDate:[myDatePicker date]];
        [startDateButton setTitle:dateString forState:UIControlStateNormal];
    }
    if ((currentPicker == policyTermPicker) && policyTermString) {
        [policyTermButton setTitle:policyTermString forState:UIControlStateNormal];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    } else {
        [self hideMyPickerView];
    }
}

#pragma mark - UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual:accountListPicker]) {
        selectedAccount = (MMSF_Object *)[accountListArray objectAtIndex:row];
    } else if ([pickerView isEqual:policyListPicker]) {
        selectedPolicy = (MMSF_Object *)[policyListArray objectAtIndex:row];
    } else if ([pickerView isEqual:policyTermPicker]) {
        policyTermString = (NSString *)[policyTermArray objectAtIndex:row];
    }
}


#pragma mark - UIPickerViewDataSource

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *returnStr = @"";
	if ([pickerView isEqual:accountListPicker]) {
        returnStr = [(MMSF_Object *)[accountListArray objectAtIndex:row] valueForKey:@"Name"];
    } else if ([pickerView isEqual:policyListPicker]) {
        returnStr = [(MMSF_Object *)[policyListArray objectAtIndex:row] valueForKey:@"Name"];
    } else if ([pickerView isEqual:policyTermPicker]) {
        returnStr = [NSString stringWithFormat:@"%@ Months",[policyTermArray objectAtIndex:row]];
    }
	return returnStr;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat componentWidth = 0.0;
    
    if ([pickerView isEqual:accountListPicker]) {
        componentWidth = 320;
    } else if ([pickerView isEqual:policyListPicker]) {
        componentWidth = 320;
    } else {
        componentWidth = 320;
    }
    
	return componentWidth;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger count;
    if ([pickerView isEqual:accountListPicker]) {
        count = [accountListArray count];
    } else if ([pickerView isEqual:policyListPicker]) {
        count = [policyListArray count];
    } else if ([pickerView isEqual:policyTermPicker]) {
        count = [policyTermArray count];
    } else {
        count = 0;
    }
    return count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}


#pragma mark - UITextFieldDelegate
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField isEqual:sumAssuredField]) {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if(theTextField == sumAssuredField) {
        [sumAssuredField resignFirstResponder];
        [aScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}


@end
